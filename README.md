# ABA English Software Guidebook

<p align="right">
   <img src="https://img.shields.io/badge/doc%20status-brain%20dump-orange.svg?style=flat" alt="doc status | brain dump">
</p>

| :warning: |
| :------------- |
| Este documento se encuentra en modo de **"brain dump"**. Durante el período de braindump iremos dejando aquí pequeños recordatorios de todo los que hace falta documentar. La idea es que el documento final este escrito todo en inglés, pero braindump lo podemos hacer en cualquier idioma. |

This is the software guidebook for the ABA English software development team.

This guide is based upon the concept of a [software guidebook](https://leanpub.com/software-architecture-for-developers/read#software-guidebook) as described in the [Software Architecture for Developers](https://leanpub.com/software-architecture-for-developers) book by [Simon Brown](http://www.simonbrown.je/), which is available on Leanpub. The software guidebook is a lightweight, pragmatic way to document the "big picture" of a software system.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Context](#context)
  - [Users](#users)
  - [External Systems](#external-systems)
- [Functional Overview](#functional-overview)
  - [Modules](#modules)
  - [Competitors and References](#competitors-and-references)
- [Quality Attributes](#quality-attributes)
  - [Performance](#performance)
  - [Scalability](#scalability)
  - [Security](#security)
  - [Availability](#availability)
  - [Internationalisation (i18n)](#internationalisation-i18n)
  - [Localisation (L10n)](#localisation-l10n)
  - [Compatibility](#compatibility)
- [Constraints](#constraints)
- [Principles](#principles)
  - [Twelve-Factor App](#twelve-factor-app)
  - [You Build iIt, You Run It](#you-build-iit-you-run-it)
  - [Continuous integration (on our way to Continuous Delivery and Continuous Deployment)](#continuous-integration-on-our-way-to-continuous-delivery-and-continuous-deployment)
  - [Visibility](#visibility)
  - [Coding the Architecture & Infrastructure as Code](#coding-the-architecture--infrastructure-as-code)
  - [“There is a SaaS for that” or “There is an API for that”](#there-is-a-saas-for-that-or-there-is-an-api-for-that)
  - [Optimize for speed of development](#optimize-for-speed-of-development)
  - [Self-Organisation](#self-organisation)
  - [Generalising Specialists](#generalising-specialists)
  - [Automated testing](#automated-testing)
  - [Static analysis tools](#static-analysis-tools)
  - [Convention over Configuration](#convention-over-configuration)
- [Software Architecture](#software-architecture)
  - [Components Diagram](#components-diagram)
  - [Container Diagram](#container-diagram)
- [Sub-platforms](#sub-platforms)
  - [ABA Platform](#aba-platform)
  - [Mobile apps](#mobile-apps)
  - [Website, landings and blog](#website-landings-and-blog)
  - [Cross-requirements](#cross-requirements)
- [Development](#development)
  - [How to code in ABA English](#how-to-code-in-aba-english)
  - [Operation & Support](#operation--support)
- [Triage](#triage)
- [Bibliography and References](#bibliography-and-references)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

This software guidebook provides an overview of the ABA English software platform. It includes a summary of the following:

1. The requirements, constraints and principles behind the platform.
2. The software architecture, including the high-level technology choices and structure of
the software.
3. The infrastructure architecture and how the software is deployed.
4. Operational and support aspects of all the systems involved.


## Context

ABA English is a subscription-based English language education platform.
ABA English mission is to democratize the learning of English in the world providing online education at accessible prices.
As a software development team our mission is to create solutions that increase the efficiency and success rate of learning the English language.

Here’s a context diagram that provides a visual summary of this:

[TBD]

The purpose of the software platform is to:

1. [TBD]
2. [TBD]

### Users

The platform provides services for internal (ABA employees) and external (mainly students) users.

There are three types of external users:

1. Anonymous: anybody with a web browser can access content on the public website and the blogs.
2. Free: registered users can sign-in to the platform and access one tier of content for free (access to premium features is restricted, obviously :)
3. Premium: have access to the whole course and enjoy all the ABA Films and Video Classes. All our course levels are available to them.
4. _Organizations_, which contract ABA English for its employees.
5. _Employees of organizations_, using ABA English course but not paying for it.

ABA employees are distributed in different roles:

1. Teacher 
2. Support Agent
3. Finance
4. Data Analyst
5. Revenue & Product manager
6. Media buyers (Acquisition manager)
7. B2B account managers
8. Admins

### External Systems

There are many external systems that the platform integrates with. These are represented by dashed grey boxes on the context diagram.

* Adjust
* CoolaData
* Facebook
* Google Analytics
* Google Tag Manager
* iTunes Connect, Google PlayStore, Google Payments
* LinkedIn
* Mixpanel
* myDBR
* Payments Gateways (Adyen, Sermepa, PayPal, Stripe, AllPago)
* Selligent
* trx.abaenglish.com
* Upsight
* Vimeo, Vzaar
* Zendesk
* Zuora


## Functional Overview

###  Modules

![Software Modules as of 20160714](assets/imgs/software_modules_2016.png)

Rounded-like shapes represents modules developed inside the company.

Square-like shapes represents modules developed outside the company.

Dotted lines means modules currently being modified

* Event Tracking and Analytics: CoolaData http://www.cooladata.com/
* HelpCenter: Zendesk http://zendesk.com/
* Emailing, Promotions and User Segmentation: Selligent http://www.selligent.com
* Payments & Subscription mngmt: Zuora https://www.zuora.com/
* Pixel Tracking mngmt: Google Tag Manager: https://www.google.es/analytics/tag-manager/
* WWW website and landings: Runroom has an external provider
* Mobile analytics and Push Notifications: Mixpanel https://mixpanel.com/ and Upsight http://www.upsight.com/
* Mobile App Tracking & Attribution: Adjust https://www.adjust.com/

##### Web, Blogs & landings

Completely public access content (commercial, promotional and social media). Acquisition of organic traffic, paid traffic. Public relations, brand image.

##### Tracking & Attribution

User's acquisition campaigns management and follow-up using pixels.

#### Course

Course content and study material (Levels, Units and Sections) with an API for mobile.

##### Emailing & Promotions

Mailing and other marketing promotions to engage user and convert them to Premium. 

##### Payments & Subscriptions (aka: Subscription Billing Management)

* Product Catalog Management
* Pricing Strategy Management
* Automated Renewals System
* Self-service Upgrades/Downgrades with proration and billing days

##### Help center

Management of support tickets, including statistics. Management of Support channels (telephone, email and chat). Management of support agents.

##### Internal messaging

Multi-platform messaging service and internal communication, primarily between teachers and student.

##### Follow-up & Auditing

Monitoring of the minimum data of the user's progress, required to allow him to continue in his learning itinerary.

##### Event tracking and analytics

Tracking of common and custom events obtaining page analytics.

##### Reporting

Revenue, course and other business reports.

##### Back-office (Intranet)

Main dashboard that integrates with all the backoffices of all other modules. Backoffice of system configuration and feature toggles.

##### ABA for Business (Corporate)

Management of the B2B corporate customers and its students.

### Competitors and References

##### Language learning apps:

* http://livemocha.com/
* https://www.babbel.com/
* https://www.duolingo.com/
* https://www.busuu.com/
* https://lingualift.com/ (estos estaban usando recurly)
* https://www.kickstarter.com/projects/54908586/fluent-panda-forget-about-forgetting-languages/description

##### Subscription content apps:

* http://www.lumosity.com/
* http://elevateapp.com/
* https://www.headspace.com/
* http://www.calm.com/


## Quality Attributes
This section provides information about the desired quality attributes (non-functional requirements) of the ABA English platform.

### Performance
We expect to serve content in a time no superior to five seconds no matter the number of users connected.

### Scalability
The ABA English software platform has to be scalable both vertically and horizontally.

### Security
Our platform will use oauth2 + JWS to secure the access of our users to all our services.

### Availability
The ABA English software platform must be always up and running. To accomplish that our platform will perform [Blue-green deployments](http://martinfowler.com/bliki/BlueGreenDeployment.html).

### Internationalisation (i18n)
All user interface text will be presented in user's language based on the browser or by GeoIP.

### Localisation (L10n)
All information will be formatted for the user locale based in the location of the browser or by GeoIP.

### Compatibility
All websites should work consistently across the following browsers:
* Safari
* Firefox
* Chrome
* Internet Explorer 9 (and above)

All mobile apps should work consistently across the following platforms:
* iOS (9.0 or later), compatible with iPhone and iPad)
* Android


## Constraints
This section provides information about the constraints imposed on the development of the ABA English software platform.

* No impact on KPIs (aka: don’t forget the business)
    * keep the servers running
    * keep the cash flow running


## Principles
This section provides information about the principles adopted for the development of the ABA English software:

### Twelve-Factor App
All our software must follow the [Twelve-Factor App](12factor.md).

### You Build iIt, You Run It
Developers who wrote code are closely involved in deploying it and watching its behaviour in production.

### Continuous integration (on our way to Continuous Delivery and Continuous Deployment)
We want to be able to commit new features and bugfixes from develop branch to production environment through a pipeline that ensures the reliability of the code.

### Visibility
Invest early in monitoring.

### Coding the Architecture & Infrastructure as Code

### “There is a SaaS for that” or “There is an API for that”
Don’t build it in-house. Leverage existing APIs. Focus only on what’s core for the product, don’t reinvent the wheel.

### Optimize for speed of development
Remove all impediments to a faster development cycle. 

### Agile Software Development
We embrace the [Agile Manifesto](http://agilemanifesto.org/), and use frameworks such as Scrum, Kanban and Scrumban.

### Self-Organisation
High Freedom, High Responsibility

### Generalising Specialists
We take inspiration from Spotify and Netflix to shape our own technology strategy.

### Automated testing
The ABA English code must be tested. Every software must provide his unit and integration tests. Our goal is to achieve 70% code coverage for automated tests.

### Static analysis tools
The ABA English code must pass rules defined in our code quality tool.

### Convention over Configuration
ABA English platform will share common code and configuration, to avoid duplication we will use [Convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration) whenever it's possible.


## Software Architecture
This section provides an overview of the ABA English software architecture.

### Components Diagram
The following diagram shows the components that make up the ABA English software platform.
[TBD]

### Container Diagram
The following diagram shows the logical containers that make up the ABA English software platform. The diagram does not represent the physical number and location of containers (please see the [Infrastructure](#infraestructure) and [Deployment](#deployment) sections for this information).
[TBD]


## Sub-platforms
ABA English software is made up of the following sub-platform:
 
### ABA Platform
* [Version 4 (aka AbaWebapps)](subplatforms/abawebapps_v4.md)
* [Version 5 (micro-services oriented architecture)](subplatforms/new_aba_platform.md)

### Mobile apps
* [iOS](subplatforms/ios.md)
* [Android](subplatforms/android.md)
          
### Website, landings and blog
* [Website](subplatforms/website.md)
* [Landings](subplatforms/website.md)
* [Blog](subplatforms/blog.md)

### Cross-requirements
* [Analytics](subplatforms/analytics.md)


## Development

### Motto
The motto of our team is:... [TBD]
> The motto of the Twitter EE team is: “Quality, Speed, Joy”.

### How to code in ABA English
This section describes the best practices we must follow to make easy the way we show, read and share our code. Those practices apply to all repositories.

#### README.md
Every repository must have a file called README.md with information about the specific service:
* Information: Regarding the build status of the develop branch and a text describing the purpose of the service.
* Index: We use [doctoc](https://github.com/thlorenz/doctoc)
* Usage: Information about the usage of the service.
* Getting Started: A section defining how to be executed. Giving information if the service requires some of the microservices or backing services inside the minikube cluster and how to link to them.

#### CHANGELOG.md
Every repository must have a file called CHANGELOG.md to show the changes between versions:
* The versions will be ordered from newest to oldest.
* Every pull request from a feature or bugfix branch must have an entry on changelog file under Unreleased section.
* Unreleased section will change to version number before the release.
* The sections under a version will be: Added, Removed, Changed, Fixed

Example:

```markdown
# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased
### Added
- the best feature never seen.

## [1.2.0] - 2016-12-16
### Changed
- changed docker base image for microservices.

## [1.1.0] - 2016-11-23
### Added
- added new module (aba-boot-starter-cache) to activate cache.
```

Reference: [Keep a Changelog](http://keepachangelog.com/en/0.3.0/)

#### Commits
We follow some well-known strategies to push commits to the repos:

* Branch model: [Gitflow](http://nvie.com/posts/a-successful-git-branching-model/) as branching model
* Commit messages: We write commit messages based on [Git Commit Guidelines](https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md#-git-commit-guidelines) from AngularJS. To follow our guidelines you can jump to [commit rules](commits.md)

#### Pull requests
Every commit on develop branch has to be made through a pull request, these are the practices we follow to make a good pull request:
* Must close or solve a JIRA task/user story/bug.
* CHANGELOG.md has to be modified if code change.
* SonarQube has to validate the new code (coverage, code smell, bugs).
* Jenkins job has to return ok.
* We follow unique commit policy to accept pull request, that means that the pull request contains only one commit. If you pushed more commits in your branch you can [meld](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase-i) all in one commit with these commands on the feature/bugfix branch:
  * git rebase -i origin/develop
    (this command will initialize an interactive rebase regarding the remote target branch, origin/develop in this case)
  * An editor will be open, the commits that will be melt are shown in descending order, so only the first commit will be ```pick``` and the rest will be marked as ```squash```, after save and close the editor, a new open will be opened to rewrite the commit message of the new unique commit.

#### Versioning Code
We use [Semantic Versioning 2.0.0](http://semver.org/) to release our code.

#### Versioning tags
We need to create executables to be deployed in any environment.

We will set our tags matching gitflow branches as follows:
- develop -> ```x.y.z-SNAPSHOT-abbrev```
- master -> ```x.y.z``` and ```latest```
- bugfix and feature branches -> ```bugfix_????``` or ```feature_????```

#### Github repository settings
[TBD]

#### JenkinsCI jobs and Jenkinsfile
This section provides information about our continuous integration and deployment.

We create three pipelines inside a folder for every repo/project:
- Multibranch pipeline job: Creates a set of pipelines according to detected branches in one repository. You can set a regular expression to avoid branches.
- Pipeline job (relase): Job to create a release.
- Pipeline job (hotfix): Job to create a hotfix.

We can use [User Service](http://jenkins.aba.land:8080/job/Backend/job/User%20Service/) as a template for other projects. Also to share method between pipelines we use this [repository](https://github.com/abaenglish/jenkins-pipeline-library)

![](assets/imgs/continuous_integration_delivery_pipeline.png)

#### Tools
Within other tools, we use:

* [Docker](tools/docker.md)
* [Event Bus](tools/eventbus.md)
* [ngrok](tools/ngrok.md)

### Operation & Support

#### Monitoring

* http://dashboards.aba.land/ -  Dashing based information radiators.
* http://status.aba.land/ - System Status Pages based on Cachet


## Triage
One of the issues that Tech Division handles is support to the other Departments in ABA English. This is done by [Triage](triage.md).


## Bibliography and References

Available in our library:

* The Twelve-Factor App - http://12factor.net/ and [download](assets/12factor.epub)
* Software Architecture for Developers - https://leanpub.com/software-architecture-for-developers
* Clean code: A Handbook of Agile Software Craftsmanship (Robert C. Martin)
* Slack: Getting Past Burnout, Busywork, and the Myth of Total Efficiency - http://www.amazon.com/gp/product/0767907698 - If you want options, then you need slack. If you want slack, then you have to learn to notice it. It's there. This books helps you find it.
* The Pragmatic Programmer: From Journeyman to Master (Andy Hunt & Dave Thomas)
* Peopleware: Productive Projects and Teams (Tom DeMarco and Tim Lister)
* Architecture: The lost years (Robert C. Martin) [PDF](assets/architecture-the-lost-years/Architecture the lost years.pdf) [Keynote](assets/architecture-the-lost-years/Architecture the lost years.key)
* Migrating to Cloud-Native Application Architectures (Matt Stine) [PDF](assets/migrating-cloud-native-application-architectures/migrating-cloud-native-application-architectures.pdf) [epub](assets/migrating-cloud-native-application-architectures/migrating-cloud-native-application-architectures.epub) [mobi](assets/migrating-cloud-native-application-architectures/migrating-cloud-native-application-architectures.mobi)
* Integration with Telenor Digital [PDF](assets/140910_Integration_with_Telenor_Digital.pdf)
* Web API Design - Crafting Interfaces that Developers Love (Apigee - Brian Mulloy) [PDF](assets/api-design-ebook-2012-03.pdf)
* Domain-Driven Design Quickly (Eric Evan) [PDF](assets/dddquickly.pdf)
* How to split a user story (Agile for all) [PDF](assets/story-splitting-flowchart.pdf)
* techtribes.je Software Guidebook (Simon Brown) [PDF](assets/techtribesje.pdf)
* Using AWS in the context of Singapore Privacy Considerations - September 2014 (AWS) [PDF](assets/Using_AWS_in_the_context_of_Singapore_Privacy_Considerations.pdf)

Not available in our library (recommended from @jbrains):

* The Leprechauns of Software Engineering. A thorough discussion of the things we believe about software development, and specifically why we believe them even though they are not true and we know that they are not true.
* Steve McConnell, Rapid Development: Taming Wild Software Schedules.
* Steve Maguire, Writing Solid Code.
* Ron Jeffries, Ann Anderson, Chet Hendrickson. Extreme Programming Installed.
* Alan Davis, 201 Principles of Software Development.
* Gerald Weinberg, The Secrets of Consulting: A Guide to Giving and Gettig Advice Successfully.
* Naomi Karten, Managing Expectations: Working With People Who Want More, Better, Faster, Sooner, NOW!.
