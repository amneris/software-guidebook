# Git Commit Guidelines

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Commit Message Format](#commit-message-format)
- [Revert](#revert)
- [Type](#type)
- [Scope](#scope)
- [Subject](#subject)
- [Body](#body)
- [Footer](#footer)
  - [Breaking Changes](#breaking-changes)
  - [Closes](#closes)
- [Examples](#examples)
- [Fun suggestions for Scope and Subject](#fun-suggestions-for-scope-and-subject)
- [More info](#more-info)
- [Reference](#reference)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction
We have very precise rules over how our git commit messages can be formatted.  This leads to **more readable messages** that are easy to follow when looking through the **project history**. But also, we use the git commit messages to automatically **generate the change log**.

## Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special format that includes a **type**, a **scope** and a **subject**:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier
to read on GitHub as well as in various git tools.

## Revert
If the commit reverts a previous commit, it should begin with `revert: `, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

## Type
Must be one of the following:

* **feat**: A new feature
* **fix**: A bug fix
* **docs**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing
  semi-colons, etc)
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **perf**: A code change that improves performance
* **test**: Adding missing tests
* **chore**: Changes to the build process or auxiliary tools and libraries such as documentation
  generation

## Scope
The scope could be anything specifying place of the commit change. For example `$location`,
`$browser`, `$compile`, `$rootScope`, `ngHref`, `ngClick`, `ngView`, etc...

## Subject
The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot (.) at the end

## Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".

The body should include the motivation for the change and contrast this with previous behavior.

## Footer
The footer should contain any information about **Breaking Changes** and is also the place to reference GitHub, Redmine or JIRA issues that this commit **closes, fixes or resolves**.

### Breaking Changes
All breaking changes have to be mentioned in footer with the description of the change, justification and migration notes

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.

```
	BREAKING CHANGE: isolate scope bindings definition has changed and
	    the inject option for the directive controller injection was removed.
	    
	    To migrate the code follow the example below:
	    
	    Before:
	    
	    scope: {
	      myAttr: 'attribute',
	      myBind: 'bind',
	      myExpression: 'expression',
	      myEval: 'evaluate',
	      myAccessor: 'accessor'
	    }
	    
	    After:
	    
	    scope: {
	      myAttr: '@',
	      myBind: '@',
	      myExpression: '&',
	      // myEval - usually not useful, but in cases where the expression is assignable, you can use '='
	      myAccessor: '=' // in directive's template change myAccessor() to myAccessor
	    }
	    
	    The removed `inject` wasn't generaly useful for directives so there should be no code using it.
```

### Closes
Closed issues should be listed on a separate line in the footer prefixed with `closes`, `fixes` or `resolves` keyword like this:

```
closes #234
```

or in case of multiple issues:

```
resolves #123, #245, #992
```

## Examples

```
feat(tracking): Carga el webtracker.js de Selligent de manera asíncrona.

 - Cambia el viejo snippet síncrono de Selligent por uno asíncrono.   

closes #6075
```

```
docs(conventions): add conventions for git commit messages 
```

```
revert: "parse unit in course url"

This reverts commit e249b0cc6aceee3ea2fd32f76f06f10de2f2d209.
``` 

## Fun suggestions for Scope and Subject 
Consider starting **scope** or **subject**  with an applicable emoji:
  * :art: `:art:` when improving the format/structure of the code
  * :moyai: `:moyai:` when adding a new feature
  * :wrench: `:wrench:` when dealing with the toolchain (Git, Travis, etc)
  * :notebook: `:notebook:` when dealing with docs
  * :racehorse: `:racehorse:` when improving performance
  * :penguin: `:penguin:` when fixing something on Linux
  * :apple: `:apple:` when fixing something on Mac OS
  * :bug: `:bug:` when fixing a bug
  * :bomb: `:bomb:` when removing code or files
  * :white_check_mark: `:white_check_mark:` when adding tests
  * :lock: `:lock:` when dealing with security
  * :arrow_up: `:arrow_up:` when upgrading dependencies
  * :arrow_down: `:arrow_down:` when downgrading dependencies

## More info
A detailed explanation can be found in this [document][https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/].

## Reference
Based on https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md#commit