# Backend Chapter

## Temas tratados

* Seguir una standarización del código
* Análisis de los nuevos proyectos / Ayuda en el diseño de soluciones / Búsqueda de soluciones conjuntas / Mantener y defender la evolución tecnológica / Cuando planteamos soluciones a user stories específicas? / Reuniones? Cuando? Cómo?
* Miedo a molestar
* Pérdida de conocimientos tecnológicos

## Temas que quedan por tratar

### Pros de pertenecer a un chapter
* Roadmap conjunto a alto nivel y big picture
* Sesiones conjuntas de programming (peer/mob)
* Seguimos manteninedo la integridad de backend / Visión más real y general
* Compartir conocimiento / Tener foro de discusión tecnológica / Compartir soluciones que el resto implementa en sus fraternidades

### Contras de pertenecer a un chapter
* Te sientes un poco desconectado / Tareas chapter
* Poder tener conocimiento de todo que está haciendo el chapter
* Bloqueos y dependencias por falta de conocimiento
* Cómo nos organizamos a nivel de fraternidades en tareas de chapter
* AbaWebapps
* Carga de trabajo descompensada

## Conclusiones

* Haremos reunión entre Story Time y Sprint Review para trazar roadmap de funcionalidades necesarias por las fraternidades.
* Revisión de pull requests cross-team: cualquier integrante del chapter puede revisar pull requests. Las revisiones irán enfocadas a buenas prácticas, performance, etc. Idealmente los integrantes de una misma fraternidad podrán aportar revisiones más enfocadas en la lógica de negocio.
* Bajo demanda se puede realizar una reunión corta de 15 minutos para hacer comentarios con respecto a alguna pull request concreta.
* Acordamos obligatoriedad en resultados de Sonar para mantener un código limpio, aunque bajaremos las expectativas de dicho análisis.
* En las tareas TECH definiremos valor y esfuerzo para argumentar nuestras necesidades.