# Frontend Chapter

## Chapter Coordination

* At present, the meetings held for 'story time' and 'sprint planning' of all fraternities this chapter belongs to occur at the same time. As such, this chapter will briefly meet between these two meetings to coordinate any cross-fraternity work needed by the chapter during the following sprint.

* A pull request should be submitted to another frontend developer for any commit made to a frontend repository. The PR reviewer should check to ensure that submitted code is well written and that best practices are followed (i.e code is clean, design patterns used where appropriate).

* Chapter meetings can be convoked any chapter member as the need arises.

* A percentage of time (TBD) of each sprint should be dedicated to tasks that are considered beneficial to the goals of the chapter (such tasks are currently defined as 'TECH' tasks)

## Bibliography and References

* You Don't Know JS (book series) - https://github.com/getify/You-Dont-Know-JS and [download](https://www.gitbook.com/download/pdf/book/maximdenisov/you-don-t-know-js)
