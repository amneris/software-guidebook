## <a name="triplecobrolaCaixa"></a> Usuario con un pago no reflejado en la tabla Payments pero reflejado en la tabla Payments_Control_Check (La Caixa)

Cuando un usuario tiene un triple pago pero no aparece en payments. 

Primero chequear en payments con el userID en payments si aparecen los pagos que se le han efectuado.
Si no es así ir a la tabla payments_control_check y volver a revisar por el userID. Si vemos reflejado ahi más de una fila. Significa que se ha efectuado el cobro pero ha habido algún problema a la hora de hacer el registro en la tabla payments.

Para solventar este problema ejecutaremos un cron.

Para ejecutar el siguiente paso hemos de tener en cuenta de que el valor en payment_control_check el status que esta en 10 pasarlo a 0, y no ha de existir ningún paymentID (NO paymentcontrolcheckId) que coincida con el paymentcontrolCheckID.

Comprobar también el lastAction para comprobar que tipo de error ha salido.

Ejecutar el siguiente cron:

```
. /home/environment_vars; /usr/bin/php /var/app/abawebapps/campus/public/script.php ForceAbaPayment   USERID   PAYMENTCONTROLCHECKID
```

Se aconseja hacer el refund desde SERMEPA, este paso hablarlo con la persona de support dandole el PaySuppOrderId, ejemplo ```6358754e:237```. También se puede intentar hacer el refund desde la intranet.

Si por un caso se hiciera el refund desde SERMEPA se tendría que ejecutar el siguiente cron para que quedará registrado en la intranet.

```
. /home/environment_vars; /usr/bin/php  /var/app/abawebapps/intranet/public/script.php  RefundPaypalPayment  USERID   PAYMENTID   [AMOUNT EN CASO DE PARCIAL]
```

El AMOUNT solo modificarlo si el refund es parcial, sino no poner nada.


## <a name="Pagonoreflejado"></a> Pago no reflejado en la Intranet (No aparece en Payments ni en Payments_Control_Check)

Primero de todo lo que vamos hacer es buscar dicho usuario en tabla user y obtendremos su `id`, con eso iremos a la tabla payments_control_check ahi buscaremos por userId. También buscaremos en la tabla payments. Normalmente si en `p_c_c` no hay una fila en payments tampoco.

Suele pasar que aparece un row en `p_c_c` pero el cliente a reportado que se le ha cobrado más veces. Este es un caso especial donde lo que vamos a tener que hacer es duplicar la linea de `p_c_c` las veces que el usuario ha reportado las duplicaciones de cobro. 

Podemos ver en la row que tenemos en `attempts` las veces que se ha intentado hacer. Es importante ir a la BBDD de `aba_b2c_logs` y dependiendo de la pasarela mirar la tabla y verificar cuantas veces se le hizo el cobro.

Vamos a duplicar la linea las veces que sea necesaria y vamos a cambiar el id. Importante mantener los datos para soporte.

Una vez tengamos las filas creadas lo que haremos será proceder a ejecutar los crons. 

```
. /home/environment_vars; /usr/bin/php /var/app/abawebapps/campus/public/script.php ForceAbaPayment   USERID   PAYMENTCONTROLCHECKID
```
Con esto se nos generara en la tabla payments las rows. Con esto dependiendo de soporte ejecutaremos los refunds necesarios.(Los refunds los harán desde la pasarela de pagos). Y nosotros ejecutaremos los crons de refund para que quede constancia en la Intranet. 

```
. /home/environment_vars; /usr/bin/php  /var/app/abawebapps/intranet/public/script.php  RefundPaypalPayment  USERID   PAYMENTID   [AMOUNT EN CASO DE PARCIAL]
```

Con esto marcaremos los refunds de los pagos que hemos devuelto pero manualmente nosotros haremos los cancels con update de los pagos recurrentes y dejaremos solo uno.


## <a name="PaypalPaymentNotShown"></a> Paypal payment not shown in Intranet (data in `payments_control_check` but not in `payments`)

When a user has done a payment but is not reflected in its paylines, you must search in `aba_b2c.payments_control_check` using the users's id in `userId` field that the payment was done (it could have status 5 or 30). You must also search in `aba_b2c_logs.log_ipn_paypal` using the users's id in `userId` field that the payment was done (`paymentStatus` must be Completed).

Change in `aba_b2c.payments_control_check` the status of the payment to '0' for the payment line of the payment:

```
UPDATE `payments_control_check` SET `status` = '0' WHERE `id` = '9a48de53';
```

To fix this issue we must run a cronjob in order to execute all the related task related with payments (statuses, dates, renewals, invoices, etc):

```
. /home/environment_vars; /usr/bin/php /var/app/abawebapps/campus/public/script.php ForceAbaPayment USERID PAYMENTCONTROLCHECKID
```

You must check after cron execution that all data is now correct (in `aba_b2c.payments_control_check` and `aba_b2c.payments`) and that a new pending recurring payment is created in `aba_b2c.payments`. 


## <a name="cancelacionnoreflejadaPaypal"></a> Cancelación no reflejada en la Intranet, efectuado en Paypal

El primer paso de todo revisar en Paypal de que la cancelación se ha realizado con éxito.

El segundo paso irnos a la BBDD aba_b2c_logs y irnos a la tabla log_ipn_paypal, ahi buscaremos por el email del usuario. Donde nos IPN (Instant Payment Notification), nos tiene que notificar de que se ha hecho la cancelación del pago recurrente.
Si aparece la notificacion pero no esta validado en Payments

Modificaremos el status por 50 que significa cancelación y cambiaremos la fecha de startTranstaction y endTransaction de cuando apareció en la tabla anterior. También cambiaremos el cancelOrigin por 1, que significa que ha sido cambiado por el usuario.

Ejemplo:

``` 
UPDATE `payments` SET `status` = '50' ,  `dateStartTransaction` = '2015-12-10 10:08:28', `dateEndTransaction` = '2015-12-10 10:08:28', `cancelOrigin` = 1 WHERE `id` = '636c890e'; 
```


## <a name="PagonoreflejadoEmailErroneo"></a> Cobro no reflejado en la intranet (USUARIO CON UN EMAIL ERRONEO)

Un alumno hizo un pago pero no se refleja en payments. Pasos a seguir primero de todo mirar la fecha y la hora en la que se realizo la transaction. En la tabla payments revisar el dateStartTransaction y mirar si aparece algun pago en cuestión.

Si es asi mirar el userId de este usuario.
En la escena que nos ha ocurrido es que el usuario hizo un pago con otra cuenta que tenia distinta a la que estaba usando ahora.

```
yuditbeatriz90@gmail.com
yudybeatriz90@gmail.com
```
Para poder solventar este problema el lo que haremos será coger el userID del usuario que esta usando y modificarlo en payments_control_check y payments.

Ejemplo:

```
UPDATE `payments_control_check` SET `userId` = '6024699' WHERE `id` = '22e1d094';
UPDATE `payments` SET `userId` = '6024699' WHERE `id` = '22e1d094';
UPDATE `payments` SET `userId` = '6024699' WHERE `id` = '83084db1';
UPDATE `payments` SET `userId` = '6024699' WHERE `id` = 'c54a1d7c';
```


## <a name="refundNotShowSermepa"></a> Refund from Sermepa does not appear

Prior to doing anything, check in Sermepa if refund has been done with success.

Check in database `aba_b2c_logs` in table `log_reconciliation`, searching by refund/payment id (`paymentId`), if there is any ack register from Sermepa for the refund. 

In case register exists, you must check if the `status` is 50 (cancelled). In that case you must update the payment line with the values of fields `dateOperation` and `refDateEndTransaction` from table `aba_b2c_logs.log_reconciliation` into fields `dateStartTransaction` and `dateEndTransaction` from table `aba_b2c.payments`:

Example:

``` 
UPDATE `payments` SET `status` = '50' ,  `dateStartTransaction` = '2015-12-10 10:08:28', `dateEndTransaction` = '2015-12-10 10:08:28', `cancelOrigin` = 1 WHERE `id` = '636c890e'; 
```

In case register does not exists, it may be because several reasons: 
* Failure in communication with Sermepa during the refund (really strange but happens).
* Failure of reconciliation crons that gather from Sermepa every day all actions done with that payment gateway (must check if crons where working ok those days)
Check in `aba_b2c.pay_gateway_sent` and `aba_b2c.pay_gateway_response` in field `iDpayment` by the payment id to see the requests and responses done for the payment.
  

## <a name="refundManualSermepa"></a> Manual refunds to Sermepa when payline has cancel status

Sometimes payments refunds to Sermepa fail, displaying a message "Automatic refund could not be processed. Error description: Campus web service refundPayment has returned an error)" in Intranet.
 
In table `aba_2c.payments` field `status` the payment has a value 50 (Cancel).
 
Change in `aba_b2c.payments_control_check` the status of the payment to '0' for the payment line of the refund:
 
```
UPDATE `payments_control_check` SET `status` = '0' WHERE `id` = '9a48de53';
```
 
To fix this issue we must run a cronjob in order to execute all the related task related with payments (statuses, dates, renewals, invoices, etc). Take notice that even when the cron scrip is called `RefundPaypalPayment` it manages refunds from Sermepa (it's just a semantic issue):
 
```
. /home/environment_vars; /usr/bin/php  /var/app/abawebapps/intranet/public/script.php RefundPaypalPayment USERID PAYMENTID [AMOUNT IN CASE OF PARTIAL REFUNDS]
```
 
You must check after cron execution that all data is now correct (in `aba_b2c.payments`) and that the refund payline has a success status.


## <a name="AllPagoError516"></a> AllPago refunds error 516

Sometimes when doing a refund for AllPago the following error appears: "Automatic refund coulkd not be processed. Error description: 516 Payment gateway did not allow transaction. - initial and referencing channel-ids do not match."

When requesting a refund from Intranet, `Pay Supp Order` could have append a `REF` string to the end of the `Pay Supp Order Id Original`. That is, for `Pay Supp Order Id Original` ABCDE:1 it generates `Pay Supp Order` ABCDE:1REF.

The first thing to try (and ask Support Team to do it) is to use the *exact* value of `Pay Supp Order Id Original` in `Pay Supp Order`.

In case this continues to fail, you must do the refund directly in AllPago's backend. Check if in `aba_b2c.payments` the register of the refund was correctly created.


## <a name="AllPagoDuplicatedRefund"></a> Duplicated AllPago refund only shown once in Intranet

Caused by rare failure in communication with AllPAgo platform. The issue is resolved by creating a fake payment and then making a refund of that payment.

The first step is to create the fake payment. You must duplicate the repeated register of the payment in `aba_b2c.payments_control_check`. You must modify the `id` and insert any id that could be recognizable in a future. For example, for the paymemnt id ABCDEFG_1 you can use the id ABA_ABCDEFG_1. You must also set `status` to 0, delete all data in `last_action` and set `attempts` to 1.

``` 
UPDATE `payments_control_check` SET `status` = '0', `last_action` = '', `attempts` = '1' WHERE `id` = 'ABA_ABCDEFG_1'; 
```

You must run a cronjob in order to execute all the related task related with the fake payment (statuses, dates, renewals, invoices, etc):

```
. /home/environment_vars; /usr/bin/php /var/app/abawebapps/campus/public/script.php ForceAbaPayment USERID PAYMENTCONTROLCHECKID
```

You must check after cron execution that all data is now correct (in `aba_b2c.payments_control_check` the `status` must be set to the value 30) and that payment is shown in Intranet.

You must execute the refund cronjob of the fake payment just done, and check after cron execution that all data is now correct (in `aba_b2c.payments`) and that the refund payline has a success status.

```
. /home/environment_vars; /usr/bin/php  /var/app/abawebapps/intranet/public/script.php RefundPaypalPayment USERID PAYMENTID [AMOUNT IN CASE OF PARTIAL REFUNDS]
```

You must enter AllPago backend and refund the payment manually. Please, bear in mind to refund the *correct* payment in AllPago.


## <a name="AdyenAdjustPaylinesActivation"></a> Activate accounts with Adyen and readjust paylines

Given the `id` of a payment done through Adyen, the issue consists in finding the refund associated to the payment in `aba_b2c.payments`. Once found, set `userId` value to 0, so that the register is not deleted, but still remains in the database for a future reference. Invoices are still there, but because they are not matched with the user, so they disappear from lists and exports.

``` 
UPDATE `payments_control_check` SET `userId` = '0' WHERE `id` = '9a48de53'; 
```

You must check payment date in the field `dateStartTransaction` in `aba_b2c.payments` table and compare it with the `expirationDate` in `aba_b2c.user` table, matching by `userId`. If the user still has an active payment of any product of ABA English, then `expirationDate` in `aba_b2c.user` must be set to the correct date, according to the product duration, and the user type in field `userType` in `aba_b2c.user` must be set to Premium user (value 2).

``` 
UPDATE `user` SET `userType` = '2' WHERE `id` = '9a48de53'; 
```

In Intranet you should access to the users manager (menu Users/Users) search for the user requested, and then press edit button (pencil) in the search results. It will then open the user's profile view.

You must press "Save" in order to submit to Selligent all the new status of the user. Check if the value for "Last Sent to Selligent" has current time when the update of data finishes.


## <a name="AdyenRefundError500Duplicado"></a> Error 500 refund Adyen ID de pago duplicado

Al intentar hacer un refund de un pago Adyen, se ha producido un error 500 con el siguiente mensaje de error: "1062 Duplicate entry 'xxxxxxxx' for key 'PRIMARY'". Por alguna razón, el ID único de pago que se genera, se ha repetido y ha coincidido con otro ID ya existente en la base de datos. 
La solución pasa por ir a la tabla "payments_adyen_pending_refunds" y localizar los datos correspondientes al REFUND. El campo idPaymentOrigin guarda el ID del SUCCESS correspondiente. Hay que modificar el valor del campo idPayment que se corresponde con el valor del mensaje de error ('xxxxxxxx') ...  


## <a name="PaypalSkippedNotification"></a> Notificación "skipped" de PayPal que provoca un FAIL en nuestra DB

El día correspondiente a la renovación de un usuario ha llegado una notificación de Paypal que se llama “recurring_payment_skipped”. Nuestro sistema espera 48 horas para realizar la renovación y si en 48 horas no llega 
el pago entonces se lo considera “FAIL”. El pago de este usuario ha llegado pasadas las 72 horas.
Tareas posteriores a realizar: 
* Actualizar tanto la fecha de expiración del usuario como las fechas en el pago FAIL
* Cambiar FAIL por SUCCESS
* Realizar un refund desde intranet y si no se puede hacer, crear un REDFUND "manualmente"
* Crear factura correspondiente al SUCCESS


## <a name="UserDeleted"></a> Recuperar usuario eliminado

Cuando un usuario es eliminado por error y se quiere recuperar.

Primero de todo será buscar por ID o por email en la tabla aba_b2c.user al usuario:
```
SELECT * FROM `user` WHERE `email` LIKE 'email@email.com%';
```
Seguidamente, cambiamos el userType que está a 0 por 1 (free) o 2 (premium)
```
UPDATE `user` SET `userType` = '2' WHERE `id` = '_idusuario_';
```
Después modificamos el email que está con formato 'email@email.com_YYYYMMDD' dejándolo sin el '_YYYYMMDD'
```
UPDATE `user` SET `email` = 'email@email.com' WHERE `id` = '6583543';
```


## <a name="CreateInvoices"></a> Creación de una factura

En los casos como el de 'Notificación "skipped" de PayPal' o en algun caso de generación de pago o refund manuales, es necesario generar la factura correspondiente al pago. Lo primero que hay que hacer es ejecutar una consulta que recupere las facturas que faltan :

```
SELECT ui.id, ui.numberInvoice, pa.* 
FROM payments AS pa
LEFT JOIN user_invoices AS ui  ON pa.id = ui.idPayment AND DATE(ui.dateInvoice) >= '2015-01-01' 
WHERE 
    pa.`status` IN (30, 40) AND 
    DATE(pa.dateEndTransaction) >= '2015-01-01' 
    AND pa.paySuppExtId NOT IN (3, 5)
    AND ui.id IS NULL 
```
Hay que descartar del resultado los siguientes IDs de payments de prueba: 91394588 y Af07b057d_15
Para las demás lineas hay que hacer lo siguiente: 
* Recuperamos el prefijo de la factura en función del país del pago, es decir, si es país es Portugal, el prefijo de las facturas es PT. 
* Ejecutamos el siguiente INSERT teniedo en cuenta el prefijo, contador y ID de pago: 

``` 
SET @PREFIX = 'PT';
SET @ZEROUS = '-000';
SET @ID =     '109c4c94';

INSERT INTO `user_invoices` 
(`prefixInvoice`, `numInvoiceId`, `numberInvoice`, `idPayment`, `dateInvoice`, `dateFirstRequest`, `dateLastDownload`, `nameAba`, `cifAba`, `addressAba`, `addressAbaMore`, `telephoneAba`, `userName`, `userLastName`, `userStreet`, `userStreetMore`, `userVatNumber`, `userZipCode`, `userCityName`, `userStateName`, `userCountryName`, `productDesc`, `amountPrice`, `currencyTrans`, `taxRateValue`, `amountTax`, `amountPriceWithoutTax`, `usedByUser`) 
SELECT prefix, nextId, nextNumId, pa.id, DATE(pa.dateEndTransaction), pa.dateEndTransaction, pa.dateEndTransaction, 
'ENGLISH WORLDWIDE, S.L.', 'B64401482', 'c/ Aribau, 240 - 7º L', '08006 BARCELONA (SPAIN)', '+34 902 024 386', 
us.name, '', NULL, NULL, NULL, NULL, NULL, NULL, co.name_m, '', pa.amountPrice, co.ABAIdCurrency, pa.taxRateValue, pa.amountTax, pa.amountPriceWithoutTax, 0 
FROM payments AS pa 
JOIN `user` AS us ON pa.userId = us.id 
JOIN country AS co ON co.id = us.countryId 
JOIN 
(
	SELECT @PREFIX AS prefix, MAX(ui.numInvoiceId) + 1 AS nextId, CONCAT(@PREFIX, @ZEROUS, MAX(ui.numInvoiceId) + 1) AS nextNumId 
	FROM user_invoices ui 
	WHERE ui.prefixInvoice = @PREFIX
) AS t1 
WHERE pa.id = @ID 
;
```

* Ojo con el contador. Puede tener distinto número de zeros. Los datos de Aba tienen que coincidir con los datos que estan en ...config/AbaSettings.php


*1. PAISES EUROPEOS
En la tabla country, en funcion del valor del campo invoice: invoice = 1 - SE GENERA LA FACTURA CON EL PREFIJO DEL PAISES

2. IOS

Caso 1. 
Para los paisees europeos para los que se genera la factura (PASO 1. PAISES EUROPEOS) 
en el campo userName se fuerza "ITUNES SARL" y en el campo userVatNumber "LU20165772".
El prefijo SIEMPRE ES IOS para TODOS LOS PAISES

Caso 2. 
Para el resto de paises lo mismo que para todos los demas casos pero el prefijo es IOS 

3. EL RESTO DE PAISES
En la tabla country, en funcion del valor del campo invoice: invoice = 0 - SE GENERA LA FACTURA CON EL PREFIJO GENERICO "ABA"

*** No se generan las facturas para GROUPONEs ni B2Bs

*Ejemplo IOS EUROPA:
```
SET @PREFIX = 'IOS';
SET @ZEROUS = '-00';
SET @ID =     '000e80ab';

--  INSERT INTO `user_invoices` 
--  (`prefixInvoice`, `numInvoiceId`, `numberInvoice`, `idPayment`, `dateInvoice`, `dateFirstRequest`, `dateLastDownload`, `nameAba`, `cifAba`, `addressAba`, `addressAbaMore`, `telephoneAba`, `userName`, `userLastName`, `userStreet`, `userStreetMore`, `userVatNumber`, `userZipCode`, `userCityName`, `userStateName`, `userCountryName`, `productDesc`, `amountPrice`, `currencyTrans`, `taxRateValue`, `amountTax`, `amountPriceWithoutTax`, `usedByUser`) 
SELECT prefix, nextId, nextNumId, pa.id, DATE(pa.dateEndTransaction), pa.dateEndTransaction, pa.dateEndTransaction, 
'ENGLISH WORLDWIDE, S.L.', 'B64401482', 'c/ Aribau, 240 - 7º L', '08006 BARCELONA (SPAIN)', '+34 902 024 386', 
'ITUNES SARL', '', NULL, NULL, 'LU20165772', NULL, NULL, NULL, co.name_m, '', pa.amountPrice, co.ABAIdCurrency, pa.taxRateValue, pa.amountTax, pa.amountPriceWithoutTax, 0 
FROM payments AS pa 
JOIN `user` AS us ON pa.userId = us.id 
JOIN country AS co ON co.id = us.countryId 
JOIN 
(
	SELECT @PREFIX AS prefix, MAX(ui.numInvoiceId) + 1 AS nextId, CONCAT(@PREFIX, @ZEROUS, MAX(ui.numInvoiceId) + 1) AS nextNumId 
	FROM user_invoices ui 
	WHERE ui.prefixInvoice = @PREFIX
) AS t1 
WHERE pa.id = @ID 
;
```


## <a name="DevolucionAdyenNoReflejadaEnIntranet"></a> Devolucion Adyen no reflejada en Intranet

* Siempre que se trate de REFUNDS de Adyen, hay que tener en cuenta que no hay una respuesta instantánea de pasarela, es decir. Cuando hacemos el REFUND, Adyen técnicamente siempre responde "RECIBIDIO", no "REALIZADO"
Entonces, pasado X TIEMPO recibimos una notificación con la respuesta final. Puede tardar tanto segundos como días. 

* Para que se refleje el REFUND en el sistema, desde el listado de pagos hay que volver a entrar al enlace "Refund" del pago y si ya tenemos la respuesta SUCCESS, el refund se creara "automáticamente". 
Si aun no hay respuesta, aparecerá el botón "Check Refund". En caso de haber llegado una respuesta KO, el refund se desbloquea y se podría volver a hacer otra vez... 


## <a name="CancelSubcriptionAndpPurchasedNewOne"></a> Usuario cancela una subcripción y da de alta una nueva antes de que caduque la cancelada (Móvil)

* La primera opción sería indicar al usuario que en la pantalla del perfil de la aplicación móvil intentara realizar un restore purchase, con esto tendría que recuperar la comprar y dar de alta de nuevo en el sistema de Aba.
* Si la primera indicara que ya hay un usuario que tiene asociada esta compra es por que ha creado otra cuenta dado que en un principio no se tiene accesible la pantalla de compras mientras tiene una subcripción
activa, aunque sea cancelada. Que loguee con el usuario correcto y recupere su compra.


## <a name="ReflejarDevolucionesAllPago"></a> Reflejar devoluciones AllPago en intranet

* Se necesita reflejar una devolución correspondiente al pago con el ID 92f0923c 

* Como se trata de una devolución total, basta con ejecutar un job manualmente desde Rundesk:  http://crons.aba.solutions:4440/menu/home  con VPN levantada. Hay que entrar a Payments -> Commands (http://crons.aba.solutions:4440/project/Payments/command/run) y poner lo siguiente: 

En Command:   . /home/environment_vars;  /usr/bin/php  /var/app/abawebapps/intranet/public/script.php  RefundPaypalPayment 2183206  92f0923c

En Nodes:   name: localhost

2183206 = userid 
92f0923c = paymentid

Si se tratara de un refund parcial, se tendría que añadir un tercer parametro con la CANTIDAD a DEVOLVER 

En Command:   . /home/environment_vars; /usr/bin/php /var/app/abawebapps/intranet/public/script.php RefundPaypalPayment  2183206  92f0923c   545

En Nodes:   name: localhost

2183206 = userid 
92f0923c = paymentid


* Un jira de ejemplo podría ser este: https://abaenglish.atlassian.net/browse/ABAWEBAPPS-840


## <a name="EliminacionDefinitivaUsuario"></a> Eliminación completa de un usuario 

* 1. La eliminación se hace con procedure cuyo código es el siguiente: 

```
DELIMITER //
CREATE PROCEDURE `resetUserFromCampus`(IN `argUserEmail` VARCHAR(65), OUT `userId` INT)
BEGIN

DECLARE varUserId INT;

SELECT u.`id` INTO varUserId FROM `user` u WHERE u.`email`=argUserEmail;

DELETE f.* FROM followup4 f WHERE f.`userid` = varUserId;
DELETE f.* FROM followup_html4 f WHERE f.`userid`=varUserId;

DELETE p.* FROM pay_gateway_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
DELETE p.* FROM pay_gateway_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
DELETE p.* FROM pay_gateway_adyen_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);
DELETE p.* FROM pay_gateway_adyen_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);

DELETE p.* FROM payments p WHERE p.`userId`=varUserId;
DELETE p.* FROM payments_control_check p WHERE p.`userId`=varUserId;
DELETE p.* FROM payments_appstore p WHERE p.`userId`=varUserId;
DELETE p.* FROM payments_playstore p WHERE p.`userId`=varUserId;

DELETE u.* FROM user_credit_forms u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_objective u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_devices u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_address_invoice u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_locations u WHERE u.`userId`=varUserId;
DELETE up.* FROM user_promos up WHERE up.`userId`=varUserId;
DELETE up.* FROM user_experiments_variations up WHERE up.`userId`=varUserId;

DELETE c.* FROM course_var4 c WHERE c.`userId`=varUserId;

DELETE m.* FROM messages m WHERE m.receiver_id=varUserId;
DELETE m.* FROM messages m WHERE m.sender_id=varUserId;

DELETE FROM `aba_b2c_logs`.`log_user_level_change` WHERE `userId`=varUserId;

DELETE IGNORE f.* FROM followup4_30 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_31 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_32 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_33 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_34 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_35 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_36 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_37 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_38 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_39 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_40 f WHERE f.`userid`=varUserId;

DELETE IGNORE f.* FROM followup_html4_30 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_31 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_32 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_33 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_34 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_35 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_36 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_37 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_38 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_39 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_40 f WHERE f.`userid`=varUserId;

DELETE u.* FROM `user` u WHERE u.id=varUserId;

SELECT varUserId INTO userId;

END//
DELIMITER ;
```
 
* 2. Se ejecuta de siguiente manera: 

```SET @userId = 0; CALL resetUserFromCampus ('email@delusuario.com', @userId); SELECT @userId;```


## <a name="renewFailLaCaixa"></a> Process of failed renewals of La Caixa

Se pretende cada dos semanas volver a intentar renovar a todos aquellos pagos que no se pudieron renovar durante las dos últimas semanas y están como FAIL. 
A tener en cuenta: DESCARTAR LOS USUARIOS PREMIUM. Aún que un PENDING se haya convertido a FAIL, si el usuario es PREMIUM es por que ha vuelto a pagar...

PROCEDIMIENTO:

*1. Visión general*

````
SELECT p.dateToPay, p.* 
FROM payments p
JOIN `user` AS u ON p.userId = u.id
WHERE
	u.userType = '1'
	AND p.`paySuppExtId` = '1'
	AND p.`status` = 10
	AND DATE(p.`dateToPay`) BETWEEN '2016-08-01' AND '2016-08-15'
ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC
;
````

*2. Insert a payments_fails*

````
INSERT INTO payments_fails (paymentId) 
SELECT p.id 
FROM payments p
JOIN `user` AS u ON p.userId = u.id
WHERE
	u.userType = '1'
	AND p.`paySuppExtId` = '1'
	AND p.`status` = 10
	AND DATE(p.`dateToPay`) BETWEEN '2016-08-01' AND '2016-08-15'
ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC
;
````

*3. Seleccionar users*
````
SET group_concat_max_len = 1024 * 1024; 

SELECT GROUP_CONCAT(p.userId) AS userIds 
FROM payments p
JOIN `user` AS u ON p.userId = u.id
WHERE
	u.userType = '1'
	AND p.`paySuppExtId` = '1'
	AND p.`status` = 10
	AND DATE(p.`dateToPay`) BETWEEN '2016-08-01' AND '2016-08-15'
ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC
;
````

*4. Actualizar users a PREMIUM*
````
UPDATE `user` AS us 
SET us.`userType` = 2 
WHERE us.`id` IN (
    IDS USERS paso 3
)
; 
````

*5. Actualizar payments a PENDING*
````
UPDATE payments AS pa 
SET `status` = 0, cancelOrigin = NULL, dateStartTransaction = '0000-00-00 00:00:00', dateEndTransaction = '0000-00-00 00:00:00' 
WHERE pa.id IN (
	SELECT paymentId FROM payments_fails 
)
````

*6. Ejecutar "cron"*
From http://crons.aba.land:4440/ using Rundeck
Payment's project
Select from upper menu Command option

````
. /home/environment_vars;  /usr/bin/php  /var/app/abawebapps/campus/public/cron.php  RenewalPaymentCaixaFails  2016-08-01  2016-08-15

name: crons.aba.land
````
A los 5 minutos de ejecución el cron falla. Hay que mirar si el id de pago del error se ha hecho el cobro del importe (mirando tabla 'pay_gateway_response' y ver si en el id de pago en el campo "AuthorisationCode" hay algún valor, si es así, se ha realizado el cobro).
En estos pagos, hay que cambiarlos a SUCCESS y añadir la linea de PENDING del próximo pago. Después, poner el usuario a Premium con la fecha de expiración correcta, ya que por lo general estará en Free.

*7. CHECK*
````
SELECT t1.`currencyTrans`, SUM(t1.amountPrice) 
FROM (
	SELECT pa.* 
	FROM payments AS pa 
	JOIN payments_fails AS pf ON pf.`paymentId` = pa.`id` 
	WHERE pa.`status` = 30 
) AS t1 
GROUP BY t1.`currencyTrans`
;
````
