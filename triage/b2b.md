## <a name="extensionUsuario"></a> Extensión de fecha de un alumno (Moodle)
Ir a la [intranet](http://intranet.abaenglish.com/index.php?r=moodle/Userlist), seleccionar el cliente en el selector y buscar por el alumno que se quiere dar de baja. Una vez localizado, editarlo y ponerle la fecha indicada y status = 5. Finalmente, darle a 'Save'.


## <a name="dropMoodleUser"></a> Dar de baja alumno (Moodle)
Ir a la [intranet](http://intranet.abaenglish.com/index.php?r=moodle/Userlist), seleccionar el cliente en el selector y buscar por el alumno que se quiere dar de baja. Una vez localizado, editarlo y ponerle una fecha inferior a la de hoy. Finalmente, darle a 'Save'.


## <a name="modificacionLicenciaExtranet"></a> Cambiar licencia de usuario a otro

El primero de los pasos será encontrar al usuario que tiene la licencia asignada. Para eso iremos a la tabla `student` y por email buscaremos su ID. Una vez tengamos su ID buscaremos en la tabla `license_asigned` y modificaremos la fecha de eliminación `deleted` por la fecha actual y `isdeleted` por 1.

Ejemplo:

``` 
UPDATE `abaenglish_extranet`.`license_asigned` SET `deleted` = '2015-12-21 12:01:41', `isdeleted` = '1' WHERE `id` = '4392';
```

El siguiente paso sera irnos a la tabla `student_group` y buscaremos por el `id_student`. Modificaremos `expirationDate` por 2000-01-01 00:00:00.

Ejemplo:

```
UPDATE `abaenglish_extranet`.`student_group` SET `expirationDate` = '2000-01-01 00:00:00' WHERE `id` = '9075';
```

El siguiente paso será modificar la fecha de expiración del usuario de campus. Buscaremos en `aba_b2c` en la tabla `user` por el email del usuario y modificaremos `expirationDate` por la fecha de ayer. 

Ejemplo:

```
UPDATE `aba_b2c`.`user` SET `expirationDate` = '2015-12-20 00:00:00' WHERE `id` = '6536397';
```

Despues iremos a la tabla `payments` y buscaremos por `userId` donde eliminaremos las rows que tiene el usuario.

Ejemplo:

```
DELETE FROM `aba_b2c`.`payments` WHERE `id` IN ('81c7ebff','914ca6c6');
```

El siguiente paso será asignarle a dicha empresa las licencias que necesita para el otro estudiante. Para eso iremos a `license_available` y buscaremos por `idEnterprise`. Ahí asignaremos las licencias que necesitemos en `numLicenses`.

Ejemplo:

```
UPDATE `abaenglish_extranet`.`license_available` SET `numLicenses` = '1' WHERE `id` = '765';
```

El último paso de todos será ir desde la extranet a la empresa y asignarle la licencia que hemos creado al usuario.


## <a name="modifyLicenseExpirationDate"></a> Modify student's expiration date

First of all, in `abaenglish_extranet` database you must obtain the student's ID in the tables `student` using its email:

```
SELECT id FROM `abaenglish_extranet`.`student` WHERE `email` = 'student@abaenglish.com';
```

In the table `student_group` we will modify the student's expiration date for the one you need:

```
UPDATE `abaenglish_extranet`.`student_group` SET `expirationDate` = '20170-01-01 00:00:00' WHERE `id` = '12345';
```

Then in `aba_b2c` database you must update the expiration date in `user` table searching by the student's email:

```
UPDATE `aba_b2c`.`user` SET `expirationDate` = '20170-01-01 00:00:00' WHERE `email` = 'student@abaenglish.com';
```


## <a name="modifyLicenseExpirationDate"></a> Modify massive student's expiration date

We need obtain all users of the enterprise to be change in `abaenglish_extranet`:

```
SELECT * FROM `student_group` WHERE `idEnterprise` = 'xxxx' AND DATE(expirationDate) = 'yyyy-mm-dd' AND isdeleted <> 1 AND `status` <> 2;
```

Now, we modify the expiration date for all students that we obtained:

```
UPDATE  `student_group` 
SET expirationDate = 'yyyy-mm-dd 00:00:00' WHERE `idEnterprise` = 'xxxx' AND DATE(expirationDate) = 'yyyy-mm-dd' AND isdeleted <> 1 AND `status` <> 2;
```

Expiration date changed on `abaenglish_extranet`. There students have be changed in `aba_b2c`. So, first of all we obtain all emails from the students from `abaenglish_extranet`:

```
SET SESSION group_concat_max_len = 1024 * 1024;
select GROUP_CONCAT(CONCAT("'" ,s.`email`,"'")) from student s where s.`id` in (select sg.`idStudent` from `student_group` sg where sg.`idEnterprise`=xxxx AND DATE(expirationDate) = 'dddd-mm-dd' and sg.`isdeleted`<>1 AND sg.`status` <> 2);
```

Now, in `aba_b2c.users` update, from emails obtained, the expiration date:

```
UPDATE `user` SET `expirationDate` = 'yyyy-mm-dd 00:00:00' WHERE email IN (all mails obtained);

```


## <a name="unpublishLandingPage"></a> Unpublish a landing page from a partner

Landing pages are part of the ABAEnglish website (www.abaenglish.com) currently developed in WordPress.

In order to unpublish/block/delete a landing page from a partner you must log into the blog backend (www.abaenglish.com/wp-admin).

*Using Wordpress's backend*

You must find the posts of the landing page to unpublish. You must check if it is in the main blog or in the different multisites (one for each language of the website). Also, you must check if it is in the Pages, Posts, Landings, etc content module (it could be in any of them, dependinng on how it was created).

Once you have find them, you must set their status to `draft`. It is not a good idea to directly delete the post, because it will be send to Wordpress's recycling bin, with empties every month, and the landing page will be lost forever, and it could be used again in a future. 

Remember the permanent URL of the post, shown in the edit view of the post. You must use only the path part of the URL (www.abaenglish.com/landing/foobar => /landing/foobar), excluding the domain.

In the menu `Tools/Redirection`, create a new redirection to redirect users to homepage, with the following data:
* Source URL: path part of the permanent URL
* Match: URL Only
* Action: Redirect to URL
* Target URL: /

Take into acount that due to Wordpress management of URLs rewrite and redirections, you must do one redirection for /landing/foobar and another one for /landing/foobar/.

*By Database*
 
Connect to the production database server of the website (ask SysAdmin which is), and connect to the database `aba_b2c_web`.

You must find the posts of the landing page to unpublish. You must check if it is in the `wp_posts` table (main blog) or in the different multisites posts table (`wp_A_posts` where A is an integer).

Once you have find them, you must set their status to `draft`. It is not a good idea to directly delete the post, because the landing page will be lost forever, and it could be used again in a future.

```
UPDATE `aba_b2c_web `.`wp_A_posts` SET `post_status` = 'draft' WHERE `id` = '1234567890';
```

Remember the `post_name` field of the post. You must precede `/landing/` to the `post_name` field in order to obtain the path of the permanent URL of the post (foobar => /landing/foobar => /landing/foobar).

In the table `aba_b2c_web`.`wp_redirection_items` or `aba_b2c_web`.`wp_A_redirection_items` (where A is an integer), depending on which site you found the post, you must create a new redirection to redirect users to homepage:

```
INSERT INTO `aba_b2c_web `.`wp_A_redirection_items` (`id`, `url`, `regex`, `position`, `last_count`, `last_access`, `group_id`, `status`, `action_type`, `action_code`, `action_data`, `match_type`, `title`)
VALUES
	(NULL,'/landing/foobar',0,0,0,'2016-01-01 00:00:00',1,'enabled','url',301,'/','url',NULL),
	(NULL,'/landing/foobar/',0,0,0,'2016-01-01 00:00:00',1,'enabled','url',301,'/','url',NULL),
```

Take into acount that due to Wordpress management of URLs rewrite and redirections, you must do one redirection for /landing/foobar and another one for /landing/foobar/.


## <a name="transferExtranetToCampus"></a> Transferring student from Extranet to Campus

Sometimes, the students of an enterprise that uses Extranet must be transferred to Campus in order to continue their learning experience in ABAEnglish.

You must search for the student in table `aba_b2c.user` and retrieve the `id` field value. You must then set this value into the field `idStudentCampus` in table `abaenglish_extranet.student` for the register corresponding to the student in that table. Also, check that the `isdeleted`values is set to true.

```
UPDATE `abaenglish_extranet`.`student` SET `idStudentCampus` = 123456789, `isdeleted` = 1 WHERE `id` = '987654321';
```

From table `abaenglish_extranet.student` you must retrieve the level from field `idLevel` and set it in the field `currentLevel` of table `aba_b2c.user`. Take notice that when `idLevel` is -2 or -1 that means that no level has been assigned to the student, so Level 1 (Beginners) must be considered. You must also check that the field ` expirationDate` is set to a date which complies with the correct date of expiration (ask ABA Business people).

```
UPDATE `aba_b2c`.`user` SET `currentLevel` = 1 WHERE `id` = '123456789';
```

Another aspect to bear in mind is the licenses associated to the partner of the student. You could be asked to add or delete a license to the corresponding partner for that student transferred to Campus. Retrieve the enterprise of the student from the field `idEnterprise` from table `abaenglish_extranet.student` of the student's register. Then in table `abaenglish_extranet.license_available` add/subtract one license to/from field `numLicenses` and `histLicenses`.


## <a name="partnerCreation"></a> Adding a partner, coupons and landing page

Step 1. Create the partner

At Intranet go to `Flash Sales/Groupers` and click `Create new grouper`. You must introduce the grouper name, the database table name and (depending on the products offered by the partner) the prefix for the table names for each product's code. That is, if the partner offers the monthly product, then the `Monthly prefix` must have a value, for example `m` to create the table `groupertablename_m`. If the partner is not offering the product, then a `no` value must be set in the prefix.

After the creation, you must view the grouper and get the partnerId (called as `Idventas Flash Script`) and check in `Other Tools/Partner List` that the partner has been correctly created and assign the `Partner Group`.

Step 2. Coupons

In order to assign/create coupons to the partner, in Intranet navigate to `Flash Sales/Coupons` and click `Create new coupon`. Select the groupoer, product and set the coupon code.

For bulk coupon creation you can use `Flash Sales/Load coupons from a file`, select the grouper, campaign, product and date, plus upload the filenama. Take into account the file is correctly formatted with the coupons (BTW, what's the format?).
 
 Step 3. Creation of the landing page

The last step is creating the landing page in ABAEnglish website, based in WordPress. First, in `Agrupadores` click `Add agrupador` and introduce in the `Partner ID` field the id of the partner created in the Intranet. Don't forget to select the `Country`.

 To create the landing page go to `Landings` and click `Add Landing`. You must enter the title, select the `Partner`, choose the `Idioma del curso`, select `Solo en campos vacío` in `Usar contenidos por defecto` and select `Product`. Normally, landing pages offer multiple products so you must add them by clicking in `Add Another Product` and selectin it. You must algo select the `Display language`l, which is normally `es` value. It is really important to edit the URL of the landing page so it SEO frienly and meets Marketing desires. Then press `Publicar` button.
 
 
## <a name="createTeacher"></a> Creación Teacher

CAMPUS:

1. Crear o modificar usuario con el e-mail correspondiente en la tabla `"aba_b2c.user"`. Asegurarse que userType sea 4 (TEACHER)

2. Insertar un registro a la tabla `"aba_b2c.user_teacher"`. UserId es el mismo de la tabla `"aba_b2c.user"`. En el campo "photo" va el nombre de la foto (72x72) que tiene que estar en abawepapps dentro de `"themes/ABAenglish/media/images/teachers/"` (la única razon por la cual hay que hacer la subira a prod :). En los campos "mobileImage2x" y "mobileImage3x" van las imágenes que se tienen que subiar previamente a cloud de amazon, por ejemplo [http://dc1spakwio57v.cloudfront.net/images/mobile/ios/iphone/teacher/teacher_i@2x.png](http://dc1spakwio57v.cloudfront.net/images/mobile/ios/iphone/teacher/teacher_i@2x.png)

EXTRANET: 

1. Tiene que haber un registro en la tabla `"abaenglish_extranet.user"`: 

```
INSERT INTO abaenglish_extranet.user (name,surname,password,email,urlAvatar,idLanguage,lastupdated,created,deleted,isdeleted,autologincode) VALUES ('My','Teacher','nopassword','myteacher@ejemplo.com','',3,now(),now(),now(),0,'d12df711e3f3f18f8a1117c29496088c');
```

2. Tiene que haber otro registro en la tabla `"abaenglish_extranet.enterprise_user_role"` con `idRol=2` (TEACHER) y `idEnterprise=X` en función de la empresa que corresponda: 

```
INSERT INTO abaenglish_extranet.enterprise_user_role (idEnterprise,idUser,idRole,orderPref,created,deleted,isdeleted) VALUES (825,1067,2,0,now(),now(),0); 
```

* Ojo, pueden haber mas de un registro en `"abaenglish_extranet.enterprise_user_role"` con idEnterprise distintos