## <a name="emailUpdate"></a> Changes in user's email

To update an email of a user, it must be done via Intranet. Changes directly done in the database **is not** the correct procedure, because Selligent updates the user's email when saving changes in Intranet.

In Intranet you should access to the users manager (menu Users/Users) search for the user requested, and then press edit button (pencil) in the search results. It will then open the user's profile view.

You must press "Enable/disable e-mail field" in order to update the user's emails, and then press "Save". Check if the value for "Last Sent to Selligent" has current time when the update of data finishes.

Sometimes the button "Enable/disable e-mail field" is not present (we should check the heuristics to know exactly when). In that case you can change the email directly in the database but then you have to comeback to the intranet, look for the user go to her user's profile, and the press "save" in the user's profile view so the change on the email is properly propagated to Selligent.

## <a name="renovaciones"></a> Fallo Renovaciones a causa de caída de Rundeck

Cada día se ejecuta un cron por la mañana que envía a selligent la info de todos los usuarios que renovaran en 7 días.
Si alguna vez el Rundeck está caído y no se generan automáticamente los datos de los usuarios que han renovado Full Price y siguen siendo premium para Selligent, lo podemos solucionar con la siguiente query.

-- El 'dateToPay' tiene que ser 7 días después a la fecha en la que no se ha ejecutado automáticamente el cron -- 

```
SELECT DISTINCT 
	pa.`userId` AS IDUSER_WEB, us.`langEnv` AS PREF_LANG, pa.`idCountry` AS COUNTRY_IP, pa.`amountPrice` AS PRICE, 
	pa.`currencyTrans` AS CURRENCY, pa.idPeriodPay AS PRODUCT, pa.`dateToPay` AS RENOV_DT
FROM payments AS pa 
JOIN `user` AS us ON us.id = pa.`userId` 
	AND us.userType = 2 

LEFT JOIN 
(
	SELECT pa2.* 
	FROM payments AS pa2 
	JOIN 
	(
		SELECT 
			pa.`userId`, pa.`idUserCreditForm`  
		FROM payments AS pa 
		JOIN `user` AS us ON us.id = pa.`userId` 
			AND us.userType = 2 
			WHERE 
			pa.`paySuppExtId` IN (1, 4, 6, 10, 2) 
			AND pa.`status` = 30 
			AND pa.`isRecurring` = 1 
			AND pa.`isExtend` = 0 
			AND DATE(pa.`dateToPay`) BETWEEN '2016-12-09' AND '2016-12-12' 
		 	AND LENGTH(pa.id) < 10 
	) T1 
	ON pa2.userId = T1.userId 
	WHERE 
		pa2.`status` = 40 
		AND (pa2.`dateStartTransaction`) >= '2016-12-09' 
		AND pa2.`idUserCreditForm` = T1.idUserCreditForm  

) AS T2 
	ON pa.`idUserCreditForm` = T2.idUserCreditForm 

WHERE 
	pa.`paySuppExtId` IN (1, 4, 6, 10, 2) 
	AND pa.`status` = 30 
	AND pa.`isRecurring` = 1 
	AND pa.`isExtend` = 0 
	AND DATE(pa.`dateToPay`) BETWEEN '2016-12-09' AND '2016-12-12' 
 	AND LENGTH(pa.id) < 10 
 	
 	AND T2.id IS NULL 
 	
ORDER BY DATE(pa.`dateToPay`) 
;
```

Con la SELECT hemos descartado los que son FREES, hemos tenido en cuenta solo los que si renovaron (los SUCCESS) y hemos descartado los que tienen REFUND.


## <a name="progress"></a> Delete Student's progress

There ara some students who wants to begin again the entire course and still have a purchase. In this case, we will delete the Student's progress.
First, We find the progress of the student searching with "userId" in the tables 'followup4_xx' (start on 'followup4_30') and in the table 'followup4' in "aba_b2c" database.

Once we find the correct table, We have to delete all the existing lines for de userId in the tables 'followup4_xx' or 'followup4' and 'followup_html4_xx' or 'followup_html4'.

```
example
DELETE f.* FROM followup4 f WHERE f.`userid`=XXXXXXX;
DELETE f.* FROM followup_html4 f WHERE f.`userid`=XXXXXXX;
or
DELETE f.* FROM followup4_xx f WHERE f.`userid`=XXXXXXX;
DELETE f.* FROM followup_html4_xx f WHERE f.`userid`=XXXXXXX;
```

Then, We have to do the same for the table 'followup4_summary' from "aba_b2c_summary" database 

```
example
DELETE f.* FROM `followup4_summary` f where f.`userid`=XXXXXXX;
```
