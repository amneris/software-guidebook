
# Adjust

## Callbacks

### Android

| Event Name | Token | Callback |
| ------ | ----- | ----- |
| Install | | ``` http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=w_1wWyMtojWDBtZk5uvzmogYjijI4nD9pqADvXEiueJCPomgN90MacOwqGw3hwbZgRbUzoTsd9Njb8yFSZ?APPID={app_id}&APPNAME={app_name}&APPVERSION={app_version}&TRACKER={tracker}&TRACKERNAME={tracker_name}&NETWORK={network_name}&CAMPAIGN={campaign_name}&ADGROUP={adgroup_name}&CREATIVE={creative_name}&ADID={adid}&IDFA={idfa}&ANDROID_ID={android_id}&CREATED_AT={created_at}&REGION={region}&COUNTRY={country}&LANG={language}&IP_ADDRESS={ip_address}&DEVICE_NAME={device_name} ``` |
| registration | tq4t6x | ``` equal,{network_name},Instagram+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400108&signature=DUMMY_VALUE equal,{network_name},Facebook+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400108&signature=DUMMY_VALUE equal,{network_name},Off-Facebook+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400108&signature=DUMMY_VALUE equal,{network_name},Twitter+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400176&signature=DUMMY_VALUE equal,{network_name},Youtube+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400286&signature=DUMMY_VALUE http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner={campaign_name}&idSource={adgroup_name}&signature=DUMMY_VALUE equal,{network_name},Google+Universal+App+Campaigns,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400519&signature=DUMMY_VALUE equal,{network_name},Yahoo+Gemini+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400523&signature=DUMMY_VALUE https://bi71yadk24.execute-api.eu-west-1.amazonaws.com/dev/event?network_name={network_name}&campaign_name={campaign_name}&tracker_name={tracker_name}&creative_name={creative_name}&adgroup_name={adgroup_name}&android_id={android_id}&adjust_id={adid}&timestamp={click_time}&gps_adid={gps_adid}&platform=android http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=7YU7l1AkMkFmYwq0pq2OiCLjId%2BgjDvxHNJtmsv2Jo1eg64Bv%2BiYXQhdS193_eE3QCGjRlivPU?ADID={adid}&PLATFORM=android ``` |

### iOS

| Event Name | Token | Callback |
| ------ | ----- | ----- |
| Install | | ``` http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=w_1wWyMtojWDBtZk5uvzmogYjijI4nD9pqADvXEiueJCPomgN90MacOwqGw3hwbZgRbUzoTsd9Njb8yFSZ?APPID={app_id}&APPNAME={app_name}&APPVERSION={app_version}&TRACKER={tracker}&TRACKERNAME={tracker_name}&NETWORK={network_name}&CAMPAIGN={campaign_name}&ADGROUP={adgroup_name}&CREATIVE={creative_name}&ADID={adid}&IDFA={idfa}&ANDROID_ID={android_id}&CREATED_AT={created_at}&REGION={region}&COUNTRY={country}&LANG={language}&IP_ADDRESS={ip_address}&DEVICE_NAME={device_name} ```|
| register_s3_newuser | 1nenx0 | ``` equal,{network_name},Instagram+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400084&signature=DUMMY_VALUE equal,{network_name},Facebook+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400084&signature=DUMMY_VALUE equal,{network_name},Off-Facebook+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400084&signature=DUMMY_VALUE equal,{network_name},Twitter+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400087&signature=DUMMY_VALUE equal,{network_name},Youtube+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400285&signature=DUMMY_VALUE equal,{tracker},mzxqkx,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400088&signature=DUMMY_VALUE http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner={campaign_name}&idSource={adgroup_name}&signature=DUMMY_VALUE equal,{network_name},Yahoo+Gemini+Installs,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400473&signature=DUMMY_VALUE equal,{network_name},Google+Universal+App+Campaigns,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400518&signature=DUMMY_VALUE equal,{network_name},Apple+Search+Ads,http://callbacks.abaenglish.com/wsrestregister/updateattribution/?idPartner=400472&signature=DUMMY_VALUE https://bi71yadk24.execute-api.eu-west-1.amazonaws.com/dev/event?network_name={network_name}&campaign_name={campaign_name}&tracker_name={tracker_name}&creative_name={creative_name}&adgroup_name={adgroup_name}&ios_id={idfa}&adjust_id={adid}&timestamp={click_time}&platform=ios http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=7YU7l1AkMkFmYwq0pq2OiCLjId%2BgjDvxHNJtmsv2Jo1eg64Bv%2BiYXQhdS193_eE3QCGjRlivPU?ADID={adid}&PLATFORM=ios ``` |


## Tracking events

| component | page | desc | full tracker code | URL |
| ------ | ----- | ----- | ----- | ----- |
| WWW | splash page mobile | App store button | interstitial-ios-es::400030::11 | `https://app.adjust.com/hzh8ip` |
| WWW | splash page mobile | ir a la app | interstitial-ios-es::400030::12 | `https://app.adjust.com/j37b3t` | 
| Campus | splash page mobile | App store button | interstitial-ios-es::400030::13 | `https://app.adjust.com/5jq8og` | 
| Campus | splash page mobile | ir a la app | interstitial-ios-es::400030::14 | `https://app.adjust.com/uxe6ld` | 
| WWW | home |app store button | web-ios-es::400037::10 | `https://app.adjust.com/3ws2t6` | 
| ¿? | Internal page | App store Button 1 | web-ios-es::400037::11 | `https://app.adjust.com/lcqz5t` | 
| ¿? | Internal page | App store Button 2 | web-ios-es::400037::12 | `https://app.adjust.com/u46or0` | 
| ¿? | Internal page | App store Button 3 | web-ios-es::400037::13 | `https://app.adjust.com/4rupv1` | 
| ¿? | Internal page | App store Button 4 | web-ios-es::400037::14 | `https://app.adjust.com/1qpsvz` | 
