# Paypal

## HTML Button Subscribe

PayPal Payments Standard provides payment buttons as a quick and easy solution for accepting payments. Payment buttons let you securely accept credit card, debit card, and PayPal payments on your website from any mobile device, tablet, or PC. Simply log in to the PayPal website to create, and optionally customize, a payment button. Then copy and paste the payment button's HTML code snippet to your website.

https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/html_example_subscribe/

### Example

```
<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">

    <!-- Identify your business so that you can collect the payments. -->
    <input type="hidden" name="business" value="MERCHANT-ID">

    <!-- Specify a Subscribe button. -->
    <input type="hidden" name="cmd" value="_xclick-subscriptions">
    <!-- Identify the subscription. -->
    <input type="hidden" name="item_name" value="ABA English Suscription">
    <input type="hidden" name="item_number" value="DIG Weekly">

    <!-- Set the terms of the regular subscription. -->
    <input type="hidden" name="currency_code" value="EUR"> <!-- EUR/USD/MXN ... -->
    <input type="hidden" name="a3" value="5.00">
    <input type="hidden" name="p3" value="1">
    <input type="hidden" name="t3" value="M"> <!-- D/W/M/Y -->

    <!-- Set recurring payments until canceled. -->
    <input type="hidden" name="src" value="1">
    <input type="hidden" name="sra" value="1">
    <input type="hidden" name="custom" value=" 'test' : 'response' ">
    <input type="hidden" name="return" value="http://mylittleapps.abaenglish.com/paypal/kikermint.html">
    <input type="hidden" name="rm" value="1">
    <input type="hidden" name="notify_url" value="http://584d1fe6.ngrok.io/api/v1/subscriptions/ipn">


    <!-- Display the payment button. -->
    <input type="image" name="submit"
           src="https://www.sandbox.paypal.com/es_ES/ES/i/btn/btn_subscribe_LG.gif"
           alt="PayPal - The safer, easier way to pay online">
    <img alt="" width="1" height="1"
         src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
</form>
```

https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/

## IPN:

* PayPal Instant Payment Notification is a call back system that will get initiated once a transaction is completed(eg: When
ExpressCheckout completed successfully).
* You will receive the transaction related IPN variables on your IPN url that you have specified in your request.
*  You have to send these IPN variables back to PayPal system for verification. Upon verification, PayPal will send
a response string with "VERIFIED" or "INVALID".
* If your server fails to respond with a successful HTTP response, PayPal will resend this IPN either until a success is received or up to 16 times.

## When create subscription in Zuora?

Hemos creado un endpoint en Subscription Service donde todas las notficiaciones que le mandamos se le notifican a PayPal como que lo hemos recibido.

Crearemos una suscripción a Zuora cuando recibamos una notificación `subscr_payment`. Para poder vincular el pago de PayPal con Zuora necesitaremos tener el id de suscripción `subscr_id` también denominado BAID y `payer_email` el email de la persona que ha efectuado el pago, junto todo esto podremos vincularlo.

### Historical IPN

- Sandbox
https://www.sandbox.paypal.com/es/cgi-bin/webscr?cmd=_display-ipns-history
- Live
https://www.paypal.com/es/cgi-bin/webscr?cmd=_display-ipns-history

### How to run?

* IPN Listener script samples are provided for different languages.
* Deploy IPN Listener script in Cloud environment or you can expose your server port using any third party LocalTunneling software, so that you can receive PayPal IPN call back.
* Make a PayPal API call (eg: DoDirect Payment request), setting the IpnNotificationUrl field of the API request class to the url of deployed IPN Listener script.
* You will receive IPN call back from PayPal.
