# Event bus

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Event bus](#event-bus)
  - [Instalación de RabbitMQ en entornos de servidor](#instalaci%C3%B3n-de-rabbitmq-en-entornos-de-servidor)
  - [Instalación de RabbitMQ en entorno local](#instalaci%C3%B3n-de-rabbitmq-en-entorno-local)
  - [Desarrollando con RabbitMQ](#desarrollando-con-rabbitmq)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Instalación de RabbitMQ en entornos de servidor

Para la instalación de RabbitMQ usaremos Ubuntu para facilitarnos la configuración.

Estos son los comandos de instalación:
```bash
$ echo 'deb http://www.rabbitmq.com/debian/ kitten main' | sudo tee /etc/apt/sources.list.d/rabbitmq.list
$ wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -
$ sudo apt-get update
$ sudo apt-get install rabbitmq-server
$ sudo rabbitmq-plugins enable rabbitmq_management
$ sudo rabbitmqctl add_user <user> <password>
$ sudo rabbitmqctl set_user_tags <user> administrator
$ sudo rabbitmqctl set_permissions -p / <user> .* .* .*
$ sudo rabbitmqctl delete_user guest
```

## Instalación de RabbitMQ en entorno local

Cada proyecto que requiera de una cola de eventos tendrá en su README.md instrucciones de como ejecutar RabbitMQ con docker, generalmente este comando será suficiente:
```bash
docker run -p 4369:4369 -p 5671-5672:5671-5672 -p 15671-15672:15671-15672 -p 25672:25672 rabbitmq:3-management
```

De todos los puertos que se exponen, dos son los que hay que tener en cuenta:
* 5672: Este puerto es al que se deben enviar los eventos.
* 15672: Este es el puerto para acceder a la UI del RabbitMQ.

## Desarrollando con RabbitMQ

El soporte de amqp se ha añadido en la versión 1.3.5 de aba-boot.
Lo primero será añadir la dependencia aba-boot-starter-amqp a nuestro proyecto.
Si queremos crear un exchange y colas que se conecten a dicho exchange tenemos que añadir en el application.yml los siguiente datos:

```yaml
spring:
  rabbitmq:
    port: 5672                          # Puerto del rabbitmq para la conexión con amqp

amqp:
  exchange: subscription-service        # Crea un exchange
  queues:
    -
      name: user-service.subscriptions  # Crea una cola con este nombre
      exchange: subscription-service    # apuntando a este exchange
      routingKeys: ["*"]                # con este routingKey
    -
      name: subscription-service.subscriptions
      exchange: subscription-service
      routingKeys: ["*"]
```

Veremos a que puerto del host se ha asignado el 5672 del docker con ```docker ps``` y lo cambiaremos en la propiedad ```spring.rabbitmq```

Si queremos trabajar sin levantar el rabbitmq podemos añadir ```amqp.dummy: true```. Esto aplicará una implementación dummy, sin lógica, de un servidor amqp embedded.

### Clases y anotaciones importantes

Aba-boot provee de varias clases que facilitan el desarrollo y la comunicación con RabbitMQ.

* DomainEvent: Todos los eventos que van a la cola deben heredar de esta clase, que obliga a tener un atributo que implemente de DomainEventType.

* DomainEventType: Todos los eventos deben tener un tipo, los tipos estarán definidos en un enum que hereda de DomainEventType asociando de esta manera el tipo a un exchange y un routingKey.

* EventPublisher: Todas las clases encargadas de publicar eventos deben heredar de esta clase que implementa el envio de eventos a la cola basado en su DomainEventType.

* @RabbitListener: Esta anotación permite estar al tanto de los eventos que llegan a una cola, anotando un método con esta anotación e identificando la cola que queremos escuchar podremos procesar eventos de uno en uno o en paquetes.
