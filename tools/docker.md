# Docker

## Requirements

### Docker beta

[Download MAC version](https://docs.docker.com/docker-for-mac/)

### Kitematic

Using homebrew:

```
brew update
brew cask install kitematic
```

### Nexus connection

Information [here](https://github.com/abaenglish/software-guidebook/blob/master/tools/nexus.md).

```
docker login nexus.aba.land:5000
```

## Create and upload an image

Create a docker file which contains all the configutation.

Docker file documentation [here](https://docs.docker.com/engine/reference/builder/)

```
docker build <path to docker file> [-t filename]
```

Upload the image to our repository

```
docker push <image id>
```

## Get and run an image

List of uploaded images:

```
docker search nexus.aba.land:5000/subscription
```

Download an image:

```
docker pull <image id>
```

List of images:

```
docker images
```

List of containers:

```
docker ps
```

Run an image:

```
docker run -d -p <port that local machine connects to>:<port within container that app is running on> <image id>
```

_Note: -d param runs the image in background_

_Note: -p param let you choose the port (random port by default)_

## Remove data

Images

```
docker rmi <image id>
```

Containers

```
docker rm <container id>
```


