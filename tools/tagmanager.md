# Tag management

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
  - [Tag Manager account structure](#tag-manager-account-structure)
- [Website](#website)
  - [Tags](#tags)
  - [Triggers](#triggers)
  - [Variables](#variables)
- [Landings](#landings)
- [Blog](#blog)
  - [Tags](#tags-1)
  - [Triggers](#triggers-1)
  - [Variables](#variables-1)
- [Abawebapps](#abawebapps)
  - [Tags](#tags-2)
  - [Triggers](#triggers-2)
  - [Variables](#variables-2)
- [New Payment funnel](#new-payment-funnel)
  - [Tags](#tags-3)
  - [Triggers](#triggers-3)
  - [Variables](#variables-3)
- [Tracking specifications for important platforms](#tracking-specifications-for-important-platforms)
  - [Facebook](#facebook)
  - [Google Adwords](#google-adwords)
  - [Twitter](#twitter)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction 

ABA English uses many tracking pixels or tags for different purposes: attribution, tracking, testing, etc. Appart from the tags which are hard-coded in the code, to manage nearly all of them we are using [Google Tag Manager](https://tagmanager.google.com/).

### Tag Manager account structure 

The structure used by GTM is very similar to the one used by Google Analytics. Although we can manage multiple Google Tag Manage accounts, usually a single Tag Manager **account** is enough for a company since you can create multiple so called containers.

A **container** holds all the tags for a specific purpose. Although it usually equals to a single website or application, it is not needed to create separate containers for separate websites since you can control which tags to include on a single domain. Each container gets its own ID, which is a string like GTM-XXXXX where XXXXX can be any combination of letters and numbers.

Changes in a container are related to a **version**, which can be previewed and afterwards published, make it simple to switch to old versions and keep a track of your change history.  

**Tags** are created within a container. Google provides templates for its own tags and other commonly used third party solutions, but it also allows custom tags to be used. 

Every tag will be fired using a trigger. **Triggers** can be from all pages, specific URLs or events generated in code.

## Website

For ABA English's website [www.abaenglish.com](www.abaenglish.com) we are using two containers, depending of the environment:

| Container Name | Container ID | Environment | URL |
|----------------|--------------|-------------|-----|
| website-prod | GTM-MGNCJ8 | Production | www.abaenglish.com |
| website-dev | GTM-TPNX4B | Development| www.dev.abaenglish.com |

### Tags

All tags are included in GTM, except for the Google Analytics tracker. 

Documentation of the pixels used:
* New: https://docs.google.com/spreadsheets/d/1BbxYcbrS2ey27nJmb_U4cALUWP0yB79KwFE99Kqns_0
* Old: https://docs.google.com/spreadsheets/d/1Dl13Ov5qx9mm7AU0611TI4mPBY5SQSqQp43dZOvlApY

### Triggers

Tags are fired in all pages at page view, except Alexa Certify tag that requires that the webpage is loaded.

### Variables

No data layer's variables are required.

## Landings

Currently ABA English's landings are using the same container as website, because there are included in the website.

Landings in Unbounce, corresponding to domain [learn.abaenglish.com](learn.abaenglish.com), are only using website production container, due to the no sandbox or testing environment in Unbounce. 

## Blog

Currently ABA English's blog [blog.abaenglish.com](blog.abaenglish.com) is using the same container as website. 

For ABA English's blog [blog.abaenglish.com](blog.abaenglish.com) we are using two containers, depending of the environment:

| Container Name | Container ID | Environment | URL |
|----------------|--------------|-------------|-----|
| blog-prod | GTM-5BXJGDC | Production | blog.abaenglish.com |
| blog-dev | GTM-MDL5JPS | Development| devblog.abaenglish.com |

### Tags

All tags are included in GTM, except for the Google Analytics tracker. 

Documentation of the pixels used:
* New: https://docs.google.com/spreadsheets/d/1BbxYcbrS2ey27nJmb_U4cALUWP0yB79KwFE99Kqns_0
* Old: https://docs.google.com/spreadsheets/d/1Dl13Ov5qx9mm7AU0611TI4mPBY5SQSqQp43dZOvlApY

### Triggers

Tags are fired in all pages at page view, except Alexa Certify tag that requires that the webpage is loaded.

### Variables

No data layer's variables are required.

## Abawebapps

For ABA English's campus [campus.abaenglish.com](campus.abaenglish.com) we are using two containers, depending of the environment:

| Container Name | Container ID | Environment | URL |
|----------------|--------------|-------------|-----|
| abawebapps-prod | GTM-T45N7H | Production | campus.abaenglish.com |
| abawebapps-dev (formerly known as abawebapps-int)  | GTM-KMRWS6 | Development| campus.int.aba.land and campus.qa.aba.land and campus.aba.local|

There was also a container for abawebapps-dev (GTM-K2WZ3G) which was deprecated due to no real use (everyone was using abawebapps-int).

[Intranet](intranet.abaenglish.com) and [Extranet](corporate.abaenglish.com) do not use tags.

### Tags

All tags are included in GTM, except for the Google Analytics tracker, Google Analytics e-commerce tracker and Visual Web Optimizer script. 

Documentation of the pixels used:
* New: https://docs.google.com/spreadsheets/d/1BbxYcbrS2ey27nJmb_U4cALUWP0yB79KwFE99Kqns_0
* Old: https://docs.google.com/spreadsheets/d/1Dl13Ov5qx9mm7AU0611TI4mPBY5SQSqQp43dZOvlApY

There is a special tag named "NewRelic - Selligent check" for monitoring purposes, which checks if Selligent's banner is being loaded in Campus.

### Triggers

There are different triggers:

**Acquisition**

Each acquisition tag should have its corresponding trigger, with the same name. In nearly all cases, they are fired at page view in pages wich have in the URL the partner identifier (idpartner=XXX).  

**Affiliates and Content Marketing**

Each affiliate tag should have its corresponding trigger, with the same name. In nearly all cases, they are fired at page view in pages wich have in the URL the partner identifier (idpartner=XXX).  

**Login**

* *Login page* : page where login form is located (/login)
* *First login page* : page with empty partner id (?idpartner=)
 
**Revenue**

* *Payment funnel* : any of the payment funnel pages (/payments)

* *Plans page* : plans selection page (/payments/payment not containing /payments/paymentmethod)

* *Checkout page* : payment method selection and checkout page (/payments/paymentmethod or /paymentsplus/paymentmethod)

* *Confirmation page* : payment confirmation page (/payments/confirmedPayment)

* *Confirmation page Plus* : payment confirmation for Premium upgrading (/paymentsplus/confirmedpayment)

**Reputation**

Alexa Certify tag that requires that the webpage is loaded, instead of simnple viewed.

### Variables

Data layer variables used are:

| Variable | Description | Example |
|----------|-------------|---------|
| currency | type of currency | $ , € |
| discount | currency + price discount | $199 , 199€ |
| email | user's email | student@abaenglish.com |
| idPayment | payment unique identifiers (alphanumeric string) | 4441f40f |
| idProduct | {{country id}}_{{period days}} | 199_360 |
| loginTimes | number of logins of the user with the same cookie | 1 |
| paymentValue | price without currency | 56 |
| paySuppExtId | payment method type (1=credit card, 2=Paypal, etc) | 1, 2 |
| productPlan | number of days in the plan period | 30, 180, 360, 720 |
| promoCode | promotional code (alphanumeric string) | 2c92c0f8552e5302015530abaa915b5c |
| quantity | always 1 | 1 |
| userId | user's unique identifier (number string) | 2022466 |

## New Payment funnel 

New Payment Funnel of the ABA English's campus [campus.abaenglish.com](campus.abaenglish.com) is using two containers, depending of the environment:

| Container Name | Container ID | Environment | URL |
|----------------|--------------|-------------|-----|
| Payment Funnel Pro | GTM-PVC3C8 | Production | premium.abaenglish.com |
| Payment Funnel QA and DEV (formerly known as abawebapps-int)  | GTM-NTHCQ4 | QA & Development | premium.dev.aba.land and premium.qa.aba.land and |

### Tags

All tags are included in GTM, except for the Google Analytics tracker and Visual Web Optimizer script. 

Documentation of the pixels used:
* New: https://docs.google.com/spreadsheets/d/1BbxYcbrS2ey27nJmb_U4cALUWP0yB79KwFE99Kqns_0
* Old: https://docs.google.com/spreadsheets/d/1Dl13Ov5qx9mm7AU0611TI4mPBY5SQSqQp43dZOvlApY

### Triggers

There are different triggers:

**Revenue**

* *View plans page* : plans selection page (custom event view_plans_page)

* *Select plan* : user selects a plan and payment method selection and checkout page is loaded (custom event select_plan)

* *Checkout Cart loaded* : analog to *Select plan* because we don't use a shopping cart (custom event checkout_cart_loaded)

* *Payment success* : payment confirmation page (custom event payment_success)

Matching between Revenue triggers of New Payment Funnel and Abawebapps:

| Triggers (abawebapps) | Triggers (New payment funnel) |
|----------|-------------|
| Payment funnel |   |
| Plans page | View plans page |
| Checkout page | Select plan & Checkout Cart loaded |
| Confirmation page | Payment success |
| Confirmation page Plus |  |

**Reputation**

Alexa Certify tag that requires that the webpage is loaded, instead of simple viewed (trigger *Windows loaded*).

**Leads**

There is also a trigger called *User login* (custom event user_login), which is not currently been called in the New Payment Funnel because users must be previously logged to use the .

### Variables

Data layer variables used are:

| Variable | Description | Example |
|----------|-------------|---------|
| currency | type of currency | $ / € |
| paymentId | payment unique identifiers (alphanumeric string) | 4441f40f |
| paymentMethodId | payment method type (1=credit card, 2=Paypal, etc) | 1= caixa; 2=paypal |
| productDiscountWithCurrency | currency + price discount | $199 / 199€ |
| productId | {{country id}}_{{period days}} | 199_360 |
| productPrice | price without currency | 56 |
| productQuantity | number of products payed | 1 |
| subscriptionPeriodDays | number of days in the plan period | 30, 180, 360, 720 |
| userEmail | user's email | student@abaenglish.com |
| userId | user's unique identifier (number string) | 2022466 |
| userLanguage | user's language ISO 639-2 code | ES, EN, FR, IT, DE, PT, RU |

Matching between data layer of New Payment Funnel and Abawebapps:

| Variable (abawebapps) | Variable (New payment funnel) |
|----------|-------------|
| currency | currency |
| discount | productDiscountWithCurrency |
| email | userEmail |
| idPayment | paymentId |
| idProduct | productId |
| loginTimes | |
| paymentValue | productPrice |
| paySuppExtId | paymentMethodId |
| productPlan | subscriptionPeriodDays |
|   | productQuantity |
| promoCode |  |
| quantity |  |
| userId | userId |
|   | userLanguage |

## Tracking specifications for important platforms

### Facebook

Facebook Pixel are managed through [Facebook Business](https://business.facebook.com/).

**Web**

Facebook pixels (previously conversion tracking pixels) are in charge of recollecting information, to track conversion and create audiences based in the information gathered. Facebook considers events such as View Content, Search Add to Cart, Add to Wishlist, Initiate Checkout, Add Payment Info, Purchase, Lead, Complete Registration and Custom ([see more details](https://developers.facebook.com/docs/marketing-api/audiences-api/pixel#standardevents)).

To set up Facebook tracking, there must be a Facebook base pixel in all pages, which is configured in Google Tag Manager **Facebook Base Code**. This tag is located in the containers for web, blog, landing pages, Abawebapps and New Payment Funnel. 

The current conversion events for Facebook are:

| Name of tag | Type of event | Trigger | Used on | Data sent |
|----|----|----|----|----|
| Facebook Base Code | ViewContent | All pages | Abawebapps, New Payment Funnel, Website, Landing pages, Blog | user's email|
| Facebook Complete Registration | CompleteRegistration | Campus First Login | Abawebapps (firing priority 1) | payment value of 1 EUR |
| Facebook Lead | Lead | Campus First Login | Abawebapps (firing priority 2) | payment value of 1 EUR |
| Facebook Add to Cart | AddToCart | Checkout page |Abawebapps, New Payment Funnel (firing priority 1) | payment value + currency |
| Facebook Initiate Checkout | InitiateCheckout | Checkout page | Abawebapps, New Payment Funnel (firing priority 2) | None |
| Facebook Purchase | Purchase | Confirmation page |Abawebapps, New Payment Funnel | payment value + currency |

**Mobile**

Facebook tracking is done in our mobile apps (iOS and Android) through Adjust and not using the Facebook Mobile SDK. Adjust receives the information from the mobile app, and then sends the information gathered to Facebook.

| Facebook Mobile event | iOs Adjust event | Android Adjust event |
|----|----|----|
| FB Mobile Activated app | | |
| FB Mobile Level Achieved | | |
| FB Mobile Add Payment Info | | |
| FB Mobile Add to Cart | | |
| FB Mobile Add to Wishlist | | |
| FB Mobile Complete Registration | REGISTER_S3_NEWUSER| REGISTRATION |
| FB Mobile Tutorial Completition | | |
| FB Mobile Initiated Checkout | | |
| FB Mobile Purchase | PURCHASE | PURCHASE |
| FB Mobile Rate | | |
| FB Mobile Search | | |
| FB Mobile Spent Credits | | |
| FB Mobile Achievement Unlocked | | |
| FB Mobile Content View | INTERACTION | INTERACTION |
  
### Google Adwords

TBD

### Twitter

Twitter tracking pixels are managed through [Twitter Ads](https://ads.twitter.com/accounts/18ce53x0xq2/conversion_tracking). 

**Web**

Twitter tracking consists in website tags (called **conversion events**) which are in charge of recollecting information, to create audiences based in these tags. Twitter considers _conversion_ to events such as Site visit, Purchase, Download, Sign up and Custom.

There must always be installed the Universal website tag, with the corresponding website tag ID, even if you create new conversion events, to maintain historical data and audiences.

Conversion events in Twitter for ABA English must follow the following naming convention:
**[[ConversionType]] _ [[Domain]] _ [[TrackerType]]**
* ConversionType: _Visit_ (for site visits), _Lead_ (for sign ups), _Premium_ (for purchases)
* Domain: which ABA English's plarform uses (web, campus, etc)
* TrackerType: _u_ for Universal website tag or _spa_ for Single event website tag

The current conversion events for Twitter are:

|Name|Type of event|Tag ID|Type of tag|Used on|
|----|----|----|----|----|
| Lead_pixel_u | Sign up | nunq6 | Universal website tag | URL contains idPartner |
| Premium_pixel_u | Purchase | nunq6 | Universal website tag | URL contains confirmedPayment|
| Premium_pixel_spa | Purchase | nw7g8 | Single event website tag | Used in New Payment Funnel|
| Visit_web_ABA_u | Site visit | nunq6 | Universal website tag | URL contains www.abaenglish.com or learn.abaenglish.com |
| Visit_campus_u | Site visit | nunq6 | Universal website tag | URL contains campus|
| Visit_blog_u | Site visit | nunq6 | Universal website tag | URL contains blog.abaenglish.com|

Audiences, generated for campaigns, from the data gatehered from pixels follow a similar naming convention to the conversion tracker: 
**[[ConversionType]] _ [[Domain]] _ audience _ [[TrackerType]]**

Current audiences in Twitter for web events are:

|Name|Type|Used on|
|----|----|----|
|Leads_audience_pixel|Website visitors - Universal|URL contains idpartner|
|Premium_audience_pixel|Website visitors - Universal|URL contains confirmedPayment|
|Premium_audience_spa|Website visitors - Single event|Used in New Payment Funnel|
|Visit_web_audience_pixel|Website visitors - Universal|URL contains www.abaenglish.com|
|Visit_campus_audience_pixel|Website visitors - Universal|URL contains campus|
|Visit_payments_audience_pixel|Website visitors - Universal|URL contains payments/payment|
|Visit_blog_audience_pixel|Website visitors - Universal|URL contains blog.abaenglish.com|
|Visit_lp_audience_pixel|Website visitors - Single event|URL contains learn.abaenglish.com|

The implementation of the convertion events are done by Google Tag Manager, using the following tags:

|Container|Tag name|Type|Twitter ID|Trigger|Purpose|
|----|----|----|----|----|----|
|abawebapps-pro|Twitter universal|Universal website tag|nunq6|All pages|Universal tag and Site Visit|
|abawebapps-pro|Twitter Single event Tag|Universal website tag|nunq6|Confirmation page|Purchase|
|website-pro|Twitter universal|Universal website tag|nunq6|All pages|Universal tag and Site Visit|
|blog-pro|Twitter universal|Universal website tag|nunq6|All pages|Universal tag and Site Visit|
|Payment Funnel Pro|Twitter universal|Universal website tag|nunq6|All pages|Universal tag and Site Visit|
|Payment Funnel Pro|Twitter Single Event Tag|Single event website tag|nw7g8|Payment success event|Purchase|

**Mobile**

Twitter tracking is done in our mobile apps (iOS and Android) through Adjust and not using the Twitter Mobile SDK. Adjust receives the information from the mobile app, and then sends the information gathered to Twitter.

| Twitter Mobile event | iOs Adjust event | Android Adjust event |
|----|----|----|
| sign_up | REGISTER_S3_NEWUSER | sign_up |
| login | LOGIN | LOGIN |
| purchase | PURCHASE | PURCHASE |
| update | | |
| level_achieved | | |
| added_payment_info | | |
| add_to_cart | | |
| add_to_wishlist | | |
| tutorial_complete | | |
| checkout_initiated | | |
| rated | | |
| content_view | | |
| achievement_unlocked | | |
| spent_credits | | |
| search | | |
| reservation | | |
| share | | |
| invite | | |
| content view | | INTERACTION | 

In Twitter the conversion events configured for apps are:

| Platform | Conversion type |
| Android | App open |
| Android | Content view |
| Android | Install |
| Android | Login |
| Android | Purchase |
| Android | Sign up |
| iOS | App open |
| iOS | Install |
| iOS | Login |
| iOS (old) | App open
| iOS (old) | Install
| iOS (old) |  Sign up

Note: iOS (old) means the iOS app named "ABA English - Aprende ingles para la vida real for iOS"

Current audiences in Twitter for mobile events are:
* Login - Learn English with Films - ABA English - iOS
* Sign up - Learn English with ABA English - Android
* Login - Learn English with ABA English - Android
* Content view - Learn English with ABA English - Android
* Purchase - Learn English with ABA English - Android
* App open - Learn English with ABA English - Android
* Install - Learn English with ABA English - Android
* Purchase - Learn English with Films - ABA English - iOS
* Sign up - Learn English with Films - ABA English - iOS
* App open - Learn English with Films - ABA English - iOS
* Install - Learn English with Films - ABA English - iOS
* App open - ABA English - Aprende inglés para la vida real - iOS
* Sign up - ABA English - Aprende inglés para la vida real - iOS
* Install - ABA English - Aprende inglés para la vida real - iOS

**Email audiences**

Twitter also allows to create audiences by adding a list of users with their email address, mobile phone number or Twitter id.

Current audiences in Twitter for uploaded lists are:
* All users
  * Users_Total (20170401)
  * Users_Total14Dec2016
  * Users_total (20160907)
  * Users_total (20160511)

* New users
  * Users_new_last2months (20170401)

* Premium users
  * Premiums (20170401)
  * Premiums_20161212
  * Premiums_20160907
  * Premiums (20160707)
  * Premiums (20160511)

* Unknown
  * free_p8_20160707