# Triage

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [B2B](#b2b)
- [Payments](#payments)
- [Users](#users)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


On Thu, Oct 15, 2015 at 10:50 AM, Mónica Giraldo <mgiraldo@abaenglish.com> wrote:
> Hi Team,
>
> Regarding Tuesday's meeting and the idea of moving small tasks or issues
> more quickly, we have created the "triage@abaenglish,com" email account. You
> can contact the teams through this email if you consider that your
> request/issue is:
> - Easy to implement (based on past experience and not on technical
> assessment).
> - An important blockage to you or your daily work.
> - A minor issue that will help you to move forward with other tasks.
> - Something unexpected with an approaching deadline.
> - Recurring issues with a log that take less a couple of hours to fix. (We
> need to start measuring the interruptions vs permanent solution).
>
> What is not considered an issue to be dealt with through this email:
> - Important issues in production, such as payment, servers, etc. These
> issues need the IT team's attention as high priority. Please use slack or
> directly contact someone from IT team.
> - Bugs detected in the site. This should maintain the same process in
> Redmine, for now.
> - Projects or issues without a clear definition.
> - Report bugs in projects that have recently launched. These issues need to
> be addressed as part of the project. Use slack or directly contact me or
> someone from IT.
> - Change requests or small projects. (Keep as usual through Redmine)
> - Adding changes to projects "in progress" or "to do". Scope changes should
> be attached to original project.
>
> Bear in mind that the goal of this new process is to help us to be more
> efficient as a company so your collaboration and commitment following these
> rules is quite important.
>
> Most likely, not all considerations are in this email, please do not
> hesitate to contact me directly or someone from the IT team to clarify other
> possible scenarios.
>
> Thanks,
> Mónica
>
> --


## B2B

* [Extensión de fecha de un alumno B2B](triage/b2b.md#extensionUsuario)
* [Dar de baja alumno Moodle](triage/b2b.md#dropMoodleUser)
* [Cambiar licencia de usuario a otro](triage/b2b.md#modificacionLicenciaExtranet)
* [Modify users expiration date](triage/b2b.md#modifyLicenseExpirationDate)
* [Unpublish a landing page from a partner](triage/b2b.md#unpublishLandingPage)
* [Transferring student from Extranet to Campus](triage/b2b.md#transferExtranetToCampus)
* [Adding a partner, coupons and landing page](triage/b2b.md#partnerCreation)
* [Cambio de nivel RTVE](https://abaenglish.atlassian.net/browse/ABAWEBAPPS-479) - Ver comentarios en: https://abaenglish.atlassian.net/browse/ABAWEBAPPS-479
* [Creación Teacher](triage/b2b.md#createTeacher)


## Payments

* [Usuario con un pago no reflejado en la tabla Payments pero reflejado en la tabla Payments_Control_Check (La Caixa)](triage/payments.md#triplecobrolaCaixa)
* [Pago no reflejado en la Intranet (No aparece en Payments ni en Payments_Control_Check)](triage/payments.md#Pagonoreflejado)
* [Paypal payment not shown in Intranet (data in Payments_Control_Check but not in Payments)](triage/payments.md#PaypalPaymentNotShown)
* [Cancelación no reflejada en la Intranet, efectuado en Paypal](triage/payments.md#cancelacionnoreflejadaPaypal)
* [Cobro no reflejado en la intranet (USUARIO CON UN EMAIL ERRONEO)](triage/payments.md#PagonoreflejadoEmailErroneo)
* [Refund from Sermepa does not appear](triage/payments.md#refundNotShowSermepa)
* [Manual refunds to Sermepa when payline has cancel status](triage/payments.md#refundManualSermepa)
* [AllPago refunds error 516](triage/payments.md#AllPagoError516)
* [Duplicated AllPago refund only shown once in Intranet](triage/payments.md#AllPagoDuplicatedRefund)
* [Activate accounts with Adyen and readjust paylines](triage/payments.md#AdyenAdjustPaylinesActivation)
* [Error 500 refund Adyen ID de pago duplicado](triage/payments.md#AdyenRefundError500Duplicado)
* [Notificación "skipped" de PayPal que provoca un FAIL en nuestra DB](triage/payments.md#PaypalSkippedNotification)
* [Creación de una factura](triage/payments.md#CreateInvoices)
* [Devolución Adyen no reflejada](triage/payments.md#DevolucionAdyenNoReflejadaEnIntranet)
* [Usuario cancela una subcripción y da de alta una nueva antes de que caduque la cancelada (Móvil)](triage/payments.md#CancelSubcriptionAndpPurchasedNewOne)
* [Reflejar Devoluciones Allpago](triage/payments.md#ReflejarDevolucionesAllPago)
* [Eliminación completa de un usuario](triage/payments.md#EliminacionDefinitivaUsuario)
* [Process of failed renewals of La Caixa](triage/payments.md#renewFailLaCaixa)


## Users

* [Changes in user's email](triage/users.md#emailUpdate)