# ABA English websites

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Context](#context)
  - [Users](#users)
- [Functional Overview](#functional-overview)
  - [Modules](#modules)
- [Quality Attributes](#quality-attributes)
  - [Performance](#performance)
  - [Scalability](#scalability)
  - [Security](#security)
  - [Availability](#availability)
  - [Internationalisation (i18n)](#internationalisation-i18n)
  - [Localisation (L10n)](#localisation-l10n)
  - [Browser compatibility](#browser-compatibility)
- [Constraints](#constraints)
- [Principles](#principles)
  - [Twelve-Factor App](#twelve-factor-app)
  - [Automated testing](#automated-testing)
  - [Static analysis tools](#static-analysis-tools)
  - [Convention over Configuration](#convention-over-configuration)
  - [Continuous integration, delivery and deployment](#continuous-integration-delivery-and-deployment)
- [Software Architecture](#software-architecture)
  - [Components Diagram](#components-diagram)
  - [Container Diagram](#container-diagram)
- [Infrastructure Architecture](#infrastructure-architecture)
  - [Live Environment](#live-environment)
- [Deployment](#deployment)
  - [Software](#software)
  - [Building](#building)
  - [Deploying](#deploying)
  - [Configuration](#configuration)
- [Development](#development)
  - [Environment setup](#environment-setup)
  - [Website content (CMS)](#website-content-cms)
  - [Landings](#landings)
- [Operation & Support](#operation--support)
  - [Starting MySQL](#starting-mysql)
  - [Starting the Web Server](#starting-the-web-server)
  - [Crons](#crons)
  - [Monitorization](#monitorization)
  - [Backups](#backups)
- [TODO](#todo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction 

This software guidebook provides an overview of the ABA English website (www.abaenglish.com) website. It includes a summary of the following:

1. The requirements, constraints and principles behind the website.
2. The software architecture, including the high-level technology choices and structure of the software.
3. The infrastructure architecture and how the software is deployed.
4. Operational and support aspects of the website.

## Context

The www.abaenglish.com website provides a way to find about ABA English and its English course, comertial content, branding, promotions and signup to the course. It also contains all the landings pages of marketing actions. There are different ways a user can reach www.abaenglish.com:

* Directly navigating to the URL www.abaenglish.com
* Organic results when searching in different web search engines
* Ads in web search engines
* Ads in other sources (webs, mobile apps)
* Traffic produced by partners
 
Here’s a context diagram that provides a visual summary of this:

![](../assets/imgs/SG_website_context.png)

The purpose of the website is to:

1. [TBD]
2. [TBD]

### Users

The website is focused in *Anonymous* users, anybody with a web browser visiting the website, having the ability to view all the content of the website. Authenticated users from Campus don't have any different feature on the website.   

### External Systems

There are two types of systems that www.abaenglish.com integrates with: AbaWebapps systems and tracking systems. These are represented by dashed grey boxes on the context diagram.

__Abawebapps__
1. *ABA English Campus*: where login and sign up takes place 
2. *Level Test*: level test and signup

__Tracking__
1. *Google Analytics*: tracking analytics
2. *Google Tag Manager*: tracking pixels

## Functional Overview

### Modules

#### ABA English website 

Website allows to customers to access to information regarding ABA English course. The website includes:
 
* explanation of ABA English course, its method, levels, teachers and  subscription
* About ABA
* features and links to the mobile apps stores where to get ABA English mobile apps (iOS and Android)  
* a redirection to the login form in the [ABA English Campus](https://campus.abaenglish.com).
* a registration form for a Free account, that when submitted connects with AbaWebapps and signups the customer, redirecting the user to the initial dashboard of the [ABA English Campus](https://campus.abaenglish.com).

#### ABA English Corporate website

Corporate website allows to corporate companies to know ABA English. The website includes:
 * a contact form which sends an email to corporate@abaenglish.com with a subject, message and email.
 * a registration form for a trial of the course, that when submitted connects with AbaWebapps and signups the company, redirecting the user to the initial dashboard of the [ABA English Corporate Campus](https://corporate.abaenglish.com).

#### Landing pages

Landing pages are the entrance point for all Marketing campaigns. They consist in a webpage with the images, text and information about the user's acquisition campaign and a signup form. Once the user has fill the form, it connects with AbaWebapps and signups the user, redirecting the user to the initial page of the [ABA English Campus](https://campus.abaenglish.com). Partner and source ids (important parameteres for traffic attribution) are propagated via GET parameters in the URL.    

## Quality Attributes

This section provides information about the desired quality attributes (non-functional requirements) of the ABA English website.

### Performance
We expect to serve pages in a time no superior to five seconds no matter the number of users connected.

### Scalability
The ABA English website has to be scalable both vertically and horizontally.

### Security
All of the ABA English website can be viewed by anonymous users, so authentication is not needed. Connection through SSL (https) is redirected to http. 
 
### Availability
The ABA English website must be always up and running. To accomplish that our platform will perform [Blue-green deployments](http://martinfowler.com/bliki/BlueGreenDeployment.html).

### Internationalisation (i18n)
All user interface text will be presented in user's language based on the browser or by IP geolocation.

### Localisation (L10n)
All information will be formatted for the user locale based in the location of the browser or by IP geolocation.

### Browser compatibility
The ABA English website should work consistently across the following browsers:
* Safari
* Firefox
* Chrome
* Internet Explorer 9 (and above)

## Constraints
This section provides information about the constraints imposed on the development of the ABA English website.

The main constraint is the reduced time and resources available from the Software Development Department (SDD). Due to this reason, Runroom was choosen as an external provider regarding the website and Unbounce regarding the landing pages.

## Principles
This section provides information about the principles adopted for the development of the ABA English website:

### Twelve-Factor App
All development in ABA English must follow the [12factor](12factor.md)

### Automated testing
All ABA English code must be tested, providing unit and integration tests. Functional tests, when UI exists, is also required. Our goal is to achieve 70% code coverage for automated tests.

### Static analysis tools
All ABA English code must pass rules defined in our code quality tool.

### Convention over Configuration
All ABA English platform will share common code and configuration, to avoid duplication we will use [Convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration) whenever it's possible.

### Continuous integration, delivery and deployment
We want to be able to commit new features and bugfixes from develop branch to production environment through a pipeline that ensures the reliability of the code.

## Software Architecture

### Components Diagram

The following diagram shows the components that make up the ABA English website.

![](../assets/imgs/SG_website_components.png)

### Container Diagram
The following diagram shows the logical containers that make up the ABA English website system. The diagram does not represent the physical number and location of containers (please see the [Infrastructure](#infraestructure) and [Deployment](#deployment) sections for this information).

![](../assets/imgs/SG_website_containers.png)

* Web Application: an Apache HTTP Server that is the single point of access for the ABA English website from the Internet.
* Relational Database: a MySQL database that stores the majority of the data behind the ABA English website.
* File System: the file system stores all assets required in the ABA English website.

## Infrastructure Architecture
This section provides information about the infrastructure architecture of the ABA English website.

### Live Environment
The live environment is hosted on [Nexica](https://www.nexica.com/), consisting in one load balancer distributing traffic to two web servers and a database server.

#### Load Balancer (1)
IP: 212.92.50.164 
Distributes traffic coming to www.abaenglish.com

#### Web servers (2)
* aba-web-01-v aka web1 (IP: 217.13.118.194, internal 172.16.1.125)
* aba-web-02-v aka web2 (IP: 217.13.118.195, internal 172.16.1.126)
 
__Features__ 
* CPU: 4 cores
* Memory: 8 GB RAM
* HDD: 50 GB
* Operating System: Linux CentOS release 5.6 (Final)
* Web Server: Apache HTTP Server 2.2.22 (Unix)

__SSL certificate__
* Type: Comodo PremiumSSL Wildcard
* Domains: *.abaenglish.com
* Issued: 05/03/2016
* Expires: 05/03/2019

#### Database server (1)
* aba-sql-02 (IP: internal 172.16.1.20)

__Features__ 
* CPU: 4 cores
* Memory: 4 GB RAM
* HDD: 30 GB
* MySql 5.1.66

#### Domains
* www.abaenglish.com -> 212.92.50.164

DNS manager: [Nexica](https://cloudmanager.nexica.com/)

### Non-live environment
ABA English website uses Vagrant to create the development environment. There is currently only one environment (dev),with all code on develop branch fitting here.

## Deployment
This section provides information about the mapping between the [software architecture](#software-architecture) and the [infrastructure architecture](#infrastructure-architecture).

### Software
This section shows the stack of languages and technologies used by ABA English website:
* PHP: 5.2.17
* Web Server: Apache HTTP Server 2.2.22 (Unix)
* MySql: 5.1.66

* CMS: Wordpress 4.1.1

### Building
There is no building strategy applied to ABA English websites.

### Deploying
Release must be done in all servers of ABA English website (currently web1, web2,web6,web7,web8), but try it first in only one of them and test (normally as webtest.abaenglish.com).

We will split the explanation into the two code component taking part in ABA English website.

#### Wordpress CMS
Although Wordpress has its own update system to apply new released versions, the code of ABA English website is located in [Github](https://github.com/abaenglish/website/).

In both servers, the folder containing the website code has not been initialized for git, so there is no possibility to use `git fetch && git pull`.
 
1. Connect to the server by SSH `sh abaadmin@web1.abaenglish.com` (take into account VPN with Nexica must be enabled)
2. cd /var/www/ww.abaenglish.com/htdocs
3. Manually change the files to be updated. 

#### Theme
Wordpress is using the theme called *aba*. The code for the theme is located in [Github](https://github.com/abaenglish/webtheme/)

In both servers, the folder containing the website code has not been initialized for git, so there is no possibility to use `git fetch && git pull`.

1. Connect to the server by SSH `sh abaadmin@web1.abaenglish.com` (take into account VPN with Nexica must be enabled)
2. cd /var/www/ww.abaenglish.com/htdocs/wp-content/themes
3. Download the tag of the theme from Github `https://github.com/abaenglish/webtheme/archive/X.Y.Z.tar.gz` (where X.Y.Z is the version to release)
4. Extract the content of the compressed file of the release `tar -zxvf {X.Y.Z.tar.gz}`
5. Check that the ownership of all files and folders correspond to abaadmin:abaadmin. Use `chown` command to make changes were needed
6. Check that the permissions of all files and folders are correct. Use `chmod` command to make changes were needed
7. Rename the current folder of the theme to a backup name `mv aba aba-A.B.C` (where A.B.C is the version of the previous theme release)
8. Rename the new release folder `mv aba-X.Y.Z aba` (where X.Y.Z is the version to release)
9. Check that changes have been made in www.abaenglish.com

__Rollback__
1. Rename the current folder of the theme to a backup name `mv aba aba-X.Y.Z` (where X.Y.Z is the version of the theme release)
2. Rename the new release folder `mv aba-A.B.C aba` (where A.B.C is the version of the previous theme release)

### Configuration
The configuration file for the web server is /etc/httpd/conf/httpd.conf. It doesn't exists a specific vhost file for the domain www.abaenglish.com

The configuration file for Wordpress is /var/www/ww.abaenglish.com/htdocs. The configuration is hardcoded in it for the different environments. The file under Github references to environment variables of the server, although they are not been used

## Development

### Code repositories
ABA English website is using two code repositories:
* [website](https://github.com/abaenglish/website/) for the Wordpress CMS code.
* [webtheme](https://github.com/abaenglish/webtheme/) for the theme code, being a submodule of the website code in the folder /wp-content/themes/ 

### Environment setup
To setup a local environment for ABAEnglish website we will use a virtual machine using Vagrant. 

__Requirements__
* Vagrant    >= 1.8.1
* Virtualbox >= 5.0

1. Clone website repository and webtheme submodule
`git clone --recursive git@github.com:abaenglish/website.git`

2. Clone website Vagrant repository
`git clone git@bitbucket.org:abaenglish/vagrant-website.git` 

3. Rename the Vagrant repository folder
`mv vagrant-website vagrant_web` 

4. Copy from [AWS S3](https://s3-eu-west-1.amazonaws.com/abaenglish/vagrant_web/bdrules.sql) the database struct and data and copy it into `vagrant_web` folder

5. Modify host file:
`sed -i "192.168.56.102 www.dev.abaenglish.com" /etc/hosts`

6. Load the virtual machine:
`cd vagrant-website && vagrant up` 

Code will be modified in `/website` foler and you could check the results in a browser at `http://www.dev.abaenglish.com` 

### Website content (CMS) 

Using Wordpress as a CMS implies that all new content, content updates and deletes are done in database only, using the [backoffice admin panel](http://www.abaenglish.com/wp-admin).

Each language in the website (Spanish, English, Italian, French, German, Portuguese and Russian) is a different site in Wordpress CMS (multi-site configuration).

### Landings

Landings are also managed through Wordpress CMS. One of the parameters to choose when creating new landings is the template to be used, which must be created in the webtheme of the website.

## Operation & Support

### Starting MySQL
MySQL is installed as a service in the database server, and should be running after a server restart. You can check this by using the following command:
`sudo netstat -tap | grep mysql`

If you need to restart MySQL, you can use the following command:
`sudo service mysql restart`

### Starting the Web Server
Apache HTTP Server is installed as a service in the web servers, and should be running after a server restart. You can check this by using the following command:
`sudo netstat -tap | grep httpd`

If you need to restart Apache, you can use the following command:
`sudo service httpd restart`

### Crons
There are no crons related with ABA English website

### Monitorization
* *[Nexica Icinga](https://monitoritzacio.nexica.com/)*
Checks the availability of resources in the servers. Can be displayed in the [IT Dashboard](http://dashboards.aba.land//it) and itsystems@abaenglish.com receives an email.
 
* *[New Relic Synthetics](https://synthetics.newrelic.com/accounts/1268965/synthetics?filters=Monitoring:Website)*
Ping checks to the website from different locations. Notification is sent to Slack channel #logs and itsystems@abaenglish.com receives an email.

### Backups
Backups are made by our IT Provider Nexica, in a daily basis for both
They can be accessed in _______ [TBD]

### Contingency
There is no contingency policy in case of service disruption

## TODO
- [ ] Website purpose
- [ ] Backups
- [ ] Level tests