# ABA English Blog

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Context](#context)
  - [Users](#users)
- [Functional Overview](#functional-overview)
- [Quality Attributes](#quality-attributes)
  - [Performance](#performance)
  - [Scalability](#scalability)
  - [Security](#security)
  - [Availability](#availability)
  - [Internationalisation (i18n)](#internationalisation-i18n)
  - [Localisation (L10n)](#localisation-l10n)
  - [Browser compatibility](#browser-compatibility)
- [Constraints](#constraints)
- [Principles](#principles)
  - [Twelve-Factor App](#twelve-factor-app)
  - [Automated testing](#automated-testing)
  - [Static analysis tools](#static-analysis-tools)
  - [Convention over Configuration](#convention-over-configuration)
  - [Continuous integration, delivery and deployment](#continuous-integration-delivery-and-deployment)
- [Software Architecture](#software-architecture)
  - [Components Diagram](#components-diagram)
  - [Container Diagram](#container-diagram)
- [Infrastructure Architecture](#infrastructure-architecture)
  - [Live Environment](#live-environment)
- [Deployment](#deployment)
  - [Software](#software)
  - [Building](#building)
  - [Deploying](#deploying)
  - [Configuration](#configuration)
- [Development](#development)
  - [Environment setup](#environment-setup)
  - [Blog content (CMS)](#blog-content-cms)
- [Operation & Support](#operation--support)
  - [Starting MySQL](#starting-mysql)
  - [Starting the Web Server](#starting-the-web-server)
  - [Crons](#crons)
  - [Monitorization](#monitorization)
  - [Backups](#backups)
- [TODO](#todo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction 

This software guidebook provides an overview of the ABA English Blog (blog.abaenglish.com). It includes a summary of the following:

1. The requirements, constraints and principles behind the blog.
2. The software architecture, including the high-level technology choices and structure of the software.
3. The infrastructure architecture and how the software is deployed.
4. Operational and support aspects of the blog.

## Context

The **ABA Journal** (blog.abaenglish.com) is a blog provides a way for ABA English and its teachers to communicate with the students and rest of potential customers. There are different ways a user can reach blog.abaenglish.com:

* Directly navigating to the URL blog.abaenglish.com
* Link in the subfooter in ABA English's webpage
* Organic results when searching in different web search engines
 
Here’s a context diagram that provides a visual summary of this:

![](../assets/imgs/SG_blog_context.png)

The purpose of the blog is to:

1. [TBD]
2. [TBD]

### Users

The blog is focused in *Anonymous* users, anybody with a web browser visiting the blog, having the ability to view all the content of the blog. Authenticated users from Campus don't have any different feature on the blog. 

### External Systems

There are different systems that blog.abaenglish.com integrates with. These are represented by dashed grey boxes on the context diagram.

__Social media__
1. *Facebook*: widget of Facebook's page plugin of ABA English's account
2. *SounCloud*: widget of SounCloud's embedded player of ABA English's account

__Tracking__
1. *Google Analytics*: tracking analytics
2. *Google Tag Manager*: tracking pixels

## Functional Overview

ABA English's blog allows the customers to access to posts of different categories (Lifestyle & Culture, Everyday & Travel, Language & Study, Business & Work) created by the Social Media team and teachers. 

Each post also shows information regarding its author, related articles and a social sharing widget. 

There can also be found in the footer of the blog the following links:
* a redirection to the login form in the [ABA English Campus](https://campus.abaenglish.com).
* a redirection for registration to a landing page in [ABA English website](http://www.abaenglish.com).
* a redirection for contacting to the [ABA English Help Center](https://abaenglish.zendesk.com/hc/en-gb).

Regarding social networks, there are also available embedded widgets from Facebook and SoundCloud in the initial page of the blog.

## Quality Attributes

This section provides information about the desired quality attributes (non-functional requirements) of the ABA English blog.

### Performance
We expect to serve pages in a time no superior to five seconds no matter the number of users connected.

### Scalability
The ABA English blog has to be scalable both vertically and horizontally.

### Security
All of the ABA English blog can be viewed by anonymous users, so authentication is not needed. Connection through SSL (https) is redirected to http. 
 
### Availability
The ABA English blog must be always up and running. To accomplish that our platform will perform [Blue-green deployments](http://martinfowler.com/bliki/BlueGreenDeployment.html).

### Internationalisation (i18n)
All user interface text will be presented in user's language based on the browser or by IP geolocation.

### Localisation (L10n)
All information will be formatted for the user locale based in the location of the browser or by IP geolocation.

### Browser compatibility
The ABA English blog should work consistently across the following browsers:
* Safari
* Firefox
* Chrome
* Internet Explorer 9 (and above)

## Constraints
This section provides information about the constraints imposed on the development of the ABA English blog.

The main constraint is the reduced time and resources available from the Software Development Department (SDD) to commit new features and changes in the blog.

## Principles
This section provides information about the principles adopted for the development of the ABA English blog:

### Twelve-Factor App
All development in ABA English must follow the [12 factor](12factor.md)

### Automated testing
All ABA English code must be tested, providing unit and integration tests. Functional tests, when UI exists, is also required. Our goal is to achieve 70% code coverage for automated tests.

### Static analysis tools
All ABA English code must pass rules defined in our code quality tool.

### Convention over Configuration
All ABA English platform will share common code and configuration, to avoid duplication we will use [Convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration) whenever it's possible.

### Continuous integration, delivery and deployment
We want to be able to commit new features and bugfixes from develop branch to production environment through a pipeline that ensures the reliability of the code.

## Software Architecture

### Components Diagram

The following diagram shows the components that make up the ABA English blog.

![](../assets/imgs/SG_blog_components.png)

### Container Diagram
The following diagram shows the logical containers that make up the ABA English blog system. The diagram does not represent the physical number and location of containers (please see the [Infrastructure](#infraestructure) and [Deployment](#deployment) sections for this information).

![](../assets/imgs/SG_blog_containers.png)

* Web Application: an Apache HTTP Server that is the single point of access for the ABA English blog from the Internet.
* Relational Database: a MySQL database that stores the majority of the data behind the ABA English blog.
* File System: the file system stores all assets required in the ABA English blog.

## Infrastructure Architecture
This section provides information about the infrastructure architecture of the ABA English blog.

### Live Environment
The live environment is hosted on [Amazon Web Services](https://aws.amazon.com/), in the production account (_abaenglish_), consisting in an Elastic Beanstalk application (load balancer and web servers), a RDS database and a CDN to distribute assets.

#### Web servers (AWS Elastic Beanstalk)
__Application__
* Application: **abablog**
* Environment name: **ablog-prod**
* Environment id: e-sprqwd5ysb
* Environment URL: https://abablog-prod.eu-west-1.elasticbeanstalk.com/
* Environment tier: Web Server (AMI 64bit Amazon Linux 2016.09 v2.3.0 running PHP 7.0)

__Instances__
* Instance type: t2.medium (CPU: 2 virtual cores, Memory: 4 GB RAM, Volume: gp2 8 GB)
* Root volume type: General Purpose (SSD)
* Availability Zones: Any
* Key pair: production
* Security group: awseb-e-sprqwd5ysb-stack-AWSEBSecurityGroup-12K3IXYDABE40

__Scaling__
* Environment type: Load balanced, auto scaling
* Number instances: 1 - 4
* Scale based on: Average network out
* Add instance when: > 6000000
* Remove instance when: < 2000000
 
__SSL certificate__
* Type: Comodo PremiumSSL Wildcard
* Domains: *.abaenglish.com
* Issued: 05/03/2016
* Expires: 05/03/2019

#### Database server (AWS RDS)
* Instance id: **aa8dibnzsxqqfh**
* Engine type: MySql
* Engine version: 5.6.27
* Endpoint: aa8dibnzsxqqfh.cjvuilzfkqgf.eu-west-1.rds.amazonaws.com:3306
* Instance type: db.t2.large (CPU: 2 virtual cores, Memory: 8 GB RAM)
* Storage: 5 GB
* Security group: rds-awseb-e-sprqwd5ysb-stack-awsebrdsdbsecuritygroup-maie63gwoc84-oexx
* Multi Availability Zone: false

#### Domains
* blog.abaenglish.com -> CNAME abablog-prod.eu-west-1.elasticbeanstalk.com

DNS manager: [Nexica](https://cloudmanager.nexica.com/)

* db-blog-pro.aba.solutions -> aa8dibnzsxqqfh.cjvuilzfkqgf.eu-west-1.rds.amazonaws.com

DNS Manager: [AWS Route 53](https://console.aws.amazon.com)

#### Assets

Assets are distributed by a Content Delivery Network, using AWS CloudFront service which distributes a bucket from AWS S3.
* Domain: contentblog.abaenglish.com
* CDN endpoint: d1zu1e2dqiuwl0.cloudfront.net 
* Bucket: contentblog.abaenglish.com.s3.amazonaws.com

### Non-live environment
ABA English blog has currently only one environment (dev), with all code on develop branch fitting here. No Vagrant, Docker or any other imaging/container tool is used for the creation of the development environment of the blog.

There is a development environment hosted on [Amazon Web Services](https://aws.amazon.com/), in the production account (_abaland_), consisting in an Elastic Beanstalk application (load balancer and web servers) and a RDS database.

#### Load balancing and server (AWS Elastic Beanstalk)
__Application__
* Application: **abablog**
* Environment name: **ablog-dev**
* Environment id: e-wpn8txmr2u
* Environment URL: https://abablog-dev.eu-west-1.elasticbeanstalk.com/
* Environment tier: Web Server (AMI 64bit Amazon Linux 2016.09 v2.3.0 running PHP 7.0)

__Instances__
* Instance type: t2.micro (CPU: 1 virtual cores, Memory: 1 GB RAM, Volume: gp2 8 GB)
* Root volume type: General Purpose (SSD)
* Availability Zones: Any
* Key pair: production
* Security group: awseb-e-wpn8txmr2u-stack-AWSEBSecurityGroup-SQ6TBGMJM57K

__Scaling__
* Environment type: Load balanced, auto scaling
* Number instances: 1 - 4
* Scale based on: Average network out
* Add instance when: > 6000000
* Remove instance when: < 2000000
 
__SSL certificate__
* Type: Comodo PremiumSSL Wildcard
* Domains: *.abaenglish.com
* Issued: 05/03/2016
* Expires: 05/03/2019

#### Database server (AWS RDS)
* Instance id: **db-blog-dev**
* Engine type: MySql
* Engine version: 5.6.27
* Endpoint: db-blog-dev.cfbep2aydaua.eu-west-1.rds.amazonaws.com:3306
* Instance type: db.m1.micro (CPU: 1 virtual cores, Memory: 10 GB RAM)
* Storage: 5 GB
* Security group: rds-db-blog, sg_aba_office
* Multi Availability Zone: false

#### Domains
* devblog.abaenglish.com -> CNAME abablog-dev.eu-west-1.elasticbeanstalk.com

DNS manager: [Nexica](https://cloudmanager.nexica.com/)

* blob-dev.aba.land -> CNAME abablog-dev.eu-west-1.elasticbeanstalk.com

DNS Manager: [AWS Route 53](https://console.aws.amazon.com)

* db-blog-dev.aba.solutions -> db-blog-dev.cfbep2aydaua.eu-west-1.rds.amazonaws.com

DNS Manager: [AWS Route 53](https://console.aws.amazon.com)

## Deployment
This section provides information about the mapping between the [software architecture](#software-architecture) and the [infrastructure architecture](#infrastructure-architecture).

### Software
This section shows the stack of languages and technologies used by ABA English blog:
* AMI: 64bit Amazon Linux 2016.09 v2.3.0 running PHP 7.0 
* PHP: 7.0.11
* Web Server: Apache HTTP Server 2.4.23 (Unix)
* Composer: 1.2.0
* MySql: 5.6.27

* CMS: Wordpress 4.5.3

### Building
There is no building strategy applied to ABA English blog.

### Deploying
Release can be done in two different ways:

#### eb deploy
Release can be done using AWS Elastic Beanstalk deployment procedures. For that reason, there is an `.ebextensions` folder in the code to include configuration and customisation files.

Although Wordpress has its own update system to apply new released versions, the code of ABA English blog is located in [Github](https://github.com/abaenglish/blog/).

1. Obtain locally the code of the blog: `git fetch && git pull`.
2. you must have installed the AWS CLI. See installation notes in [AWS Documentation](http://docs.aws.amazon.com/cli/latest/userguide/installing.html).
3. AWS credentials must be available and configured. 
4. `eb deploy` 

__Rollback__
In case of rollback, follow AWS Elastic Beanstalk deployment procedures to deploy the previous version of the environment.

#### Jenkins
Release can also be done using JenkinsCI job. There are currently two jobs configured:

* **develop** (http://jenkins.aba.land:8080/job/Websites/job/Blog/job/develop/)
Deploys automatically _development_ branch into _abablog-dev_ AWS ElasticBeanstalk environment when a commit is pushed into the branch.

* **release** (http://jenkins.aba.land:8080/job/Websites/job/Blog/job/release/)
 DEploys manually a certain tag version into _abablog_ AWS ElasticBeanstalk environment.

### Configuration
The configuration file for the web server is /etc/httpd/conf/httpd.conf. It doesn't exists a specific vhost file for the domain blog.abaenglish.com.

The configuration file for Wordpress is /var/www/httpd/.abaenglish.com/htdocs. The configuration is hardcoded in it for the different environments. The file under Github references to environment variables of the server, configured in the AWS Elastic Beanstalk:
* DB_HOST
* DB_NAME
* DB_USER
* DB_PASSWORD
* GOOGLE_TAG_MANAGER_CONTAINER_ID
* DOMAIN_CURRENT_SITE

## Development

### Code repositories
ABA English blog is using a [Github code repository](https://github.com/abaenglish/blog/).

### Environment setup
Remember that there is no Vagrant, Docker or any other imaging/container tool is used for the creation of the development environment of the blog. 

To setup a local environment for ABA English blog (considering devblog.abaenglish.local will be the domain of the local environment):

1. Clone blog repository
`git clone git@github.com:abaenglish/blog.git`

DOMAIN_CURRENT_SITE del wp-config.php 

2. Modify wp-config.php with the new DOMAIN_CURRENT_SITE value:
```php
define('DOMAIN_CURRENT_SITE', 'devblog.abaenglish.local');
```

3. Modify the snapshot of the database used for the environment by changing the   
```mysql
UPDATE wp_options SET option_value = replace(option_value, 'http://blog.abaenglish.com', 'http://devblog.abaenglish.local') WHERE option_name = 'home' OR option_name = 'siteurl'; 
UPDATE wp_posts SET guid = replace(guid, 'http://blog.abaenglish.com','http://devblog.abaenglish.local'); 
UPDATE wp_posts SET post_content = replace(post_content, 'http://blog.abaenglish.com', 'http://devblog.abaenglish.local'); 
UPDATE wp_postmeta SET meta_value = replace(meta_value,'http://blog.abaenglish.com','http://devblog.abaenglish.local'); 
UPDATE wp_options SET option_value = replace(option_value, 'http://blog.abaenglish.com', 'devblog.abaenglish.local') WHERE option_name = 'home' OR option_name = 'siteurl'; 
UPDATE wp_posts SET guid = replace(guid, 'http://blog.abaenglish.com','devblog.abaenglish.local'); 
UPDATE wp_posts SET post_content = replace(post_content, 'http://blog.abaenglish.com', 'devblog.abaenglish.local'); 
UPDATE wp_postmeta SET meta_value = replace(meta_value,'http://blog.abaenglish.com','devblog.abaenglish.local');
update wp_blogs set domain = 'devblog.abaenglish.com' where domain = 'devblog.abaenglish.local'; 
```

### Blog content (CMS) 

Using Wordpress as a CMS implies that all new content, content updates and deletes are done in database only, using the [backoffice admin panel](http://blog.abaenglish.com/wp-admin).

## Operation & Support

### Starting MySQL
AWS RDS instance must be running. Use [AWS Console](https://console.aws.amazon.com) or AWS CLI to run, restart or request any other operation.  

### Starting the Web Server
AWS Elastic Beanstalk instance must be running. Use [AWS Console](https://console.aws.amazon.com) or AWS CLI to run, restart or request any other operation.  

### Crons
There are no crons related with ABA English blog

### Monitorization
* *[AWS Cloudwatch](https://eu-west-1.console.aws.amazon.com/cloudwatch/)*
No alarms are set for the AWS Elastic Beanstalk nor AWS RDS database.
 
* *[New Relic Servers](https://rpm.newrelic.com/accounts/1268965/servers/20622199)*
Checks the availability of resources in the servers. Notification is sent to Slack channel #logs and itsystems@abaenglish.com receives an email.
  
* *[New Relic Synthetics](https://synthetics.newrelic.com/accounts/1268965/synthetics)*
Ping checks to the blog from different locations. Notification is sent to Slack channel #logs and itsystems@abaenglish.com receives an email.

### Backups
Snapshots are made to the database every day by AWS RDS. They can be accessed in AWS RDS Snapshot console](https://eu-west-1.console.aws.amazon.com/rds/home?region=eu-west-1#db-snapshots:).

### Contingency
There is no contingency policy in case of service disruption.

## TODO
- [ ] Blog purpose