# ABA Platform Version 5 (micro-services oriented architecture)

| Notice       |
| :------------- |
| Until all ABA English platform v4 (aka AbaWebapps) ends its useful life, this v5 guidebook must be read along with v4 guidebook. |

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Context](#context)
  - [Users](#users)
  - [External Systems](#external-systems)
- [Functional Overview](#functional-overview)
- [Quality Attributes](#quality-attributes)
  - [Performance](#performance)
  - [Scalability](#scalability)
  - [Security](#security)
  - [Availability](#availability)
  - [Internationalisation (i18n)](#internationalisation-i18n)
  - [Localisation (L10n)](#localisation-l10n)
  - [Browser compatibility](#browser-compatibility)
- [Constraints](#constraints)
- [Principles](#principles)
  - [Microservices](#microservices)
  - [Twelve-Factor App](#twelve-factor-app)
  - [Automated testing](#automated-testing)
  - [Static analysis tools](#static-analysis-tools)
  - [Convention over Configuration](#convention-over-configuration)
  - [Cloud Configuration Server](#cloud-configuration-server)
  - [Continuous integration, delivery and deployment](#continuous-integration-delivery-and-deployment)
- [Software Architecture](#software-architecture)
  - [Context Diagram](#context-diagram)
  - [Container Diagram](#container-diagram)
- [Infrastructure Architecture](#infrastructure-architecture)
  - [Live Environment](#live-environment)
  - [Non-live environment](#non-live-environment)
- [Deployment](#deployment)
  - [Software](#software)
  - [Building](#building)
  - [Deploying](#deploying)
  - [Configuration](#configuration)
- [Development](#development)
  - [Environment setup](#environment-setup)
  - [How to code in ABA English](#how-to-code-in-aba-english)
- [Operation & Support](#operation--support)
  - [Services](#services)
  - [Crons](#crons)
  - [Monitorization](#monitorization)
  - [Backups](#backups)
- [TODO](#todo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

This software guidebook provides an overview of the ABA English software platforms v5 (micro-services oriented architecture). It includes a summary of the following:

1. The requirements, constraints and principles behind the platform.
2. The software architecture, including the high-level technology choices and structure of
the software.
3. The infrastructure architecture and how the software is deployed.
4. Operational and support aspects of all the systems involved.


## Context

ABA English software platform v5 is moving from a monolitic architecture (mainly _abawebapps_) to a micro-services oriented architecture. The roadmap will end when all the functionality

Here’s a context diagram that provides a visual summary of this:

[TBD]

### Payments and subscriptions management

The first main functionality transformed into micro-services is Payments and Subscriptions management.

As ABA English is a subscription-based English education platform, all the subscription and payment process was made using in-house develop software, lacking several features that could boost our revenue and reduce technical risks.

[Zuora] (http://www.zuora.com) was choosen as the external tool to provide the features around *subscription, payment, billing and renewals management*. But it will no be the SSOT.

Some goals to accomplish within this project are:

- Automatize and externalized not core features of the business
- Automatized the subscription renewals
- Adquiere a tool that allow us improve our pricing strategy
- Improve revenue through renewal features as: Dunning management, Upgrates/Downgrades with proration and billing days

### Users

The platform provides services for internal (ABA employees) and external (mainly students) users.

There are three types of external users:

1. Anonymous: anybody with a web browser can access content on the public website and the blogs.
2. Free: registered users can sign-in to the platform and access one tier of content for free (access to premium features is restricted, obviously :)
3. Premium: Have access to the whole course and enjoy all the ABA Films and Video Classes. All our course levels are available to you.

ABA employeers are distributed in different roles:

1. Teacher
2. Support Agent
3. Finance
4. Data Analist
5. Revenue & Product manager
6. Media buyers (Acquisition manager)
7. B2B account managers
8. Admins

#### Payments and subscriptions management

ABA employees that will require the use of the Zuora are:

1. _Head of Monetization and team_, in order to fix product pricing strategy
2. _Finance team_, in order to obtain and use all finantial data
3. _Support agents_, in order to access all payment actions to assist customers
4. _Analysts_, in order to obtain all data required for reporting

### External Systems

There are many external systems that the platform integrates with. These are represented by dashed grey boxes on the context diagram.

* Adjust
* CoolaData
* Facebook
* Google Analytics
* Google Tag Manager
* iTunes Connect, Google PlayStore, Google Payments
* LinkedIn
* Mixpanel
* Upsight
* Vimeo, Vzaar
* Zendesk

#### Payments and subscriptions management

* Zuora
* Selligent
* Payments Gateways (Adyen, Sermepa, PayPal, Stripe, AllPago)
* Mobile apps stores (iTunes Connect, Google PlayStore, Google Payments)

We can also consider AbaWebapps v4 as an external system, because it will be required to store free users.


## Functional Overview

### Payments and subscriptions management

* Product catalog, including setting of pricing strategies by country or user segmentation
* Checkout
  * Product selection
  * Selection of payment method
  * Payment
  * Confirmation
  * Subscription
* Cohort reporting for Adquisitions team
* Finance reporting
* Customer profile management, including invoices
* Downgrade/Upgrade of product
* Renewal of subscriptions

![](../assets/imgs/SG_newplatform_subscription_flow.png)

## Quality Attributes
This section provides information about the desired quality attributes (non-functional requirements) of the ABA English platform.

### Performance
We expect to serve pages in a time no superior to five seconds no matter the number of users connected.

### Scalability
The ABA English platform has to be scalable both vertically and horizontally.

### Security
Our platform will use oauth2 + JWS to secure the access of our users to all our services.

### Availability
The ABA English platform must be always up and running. To accomplish that our platform will perform [Blue-green deployments](http://martinfowler.com/bliki/BlueGreenDeployment.html).

### Internationalisation (i18n)
All user interface text will be presented in user's language based on the browser or by GeoIP.

### Localisation (L10n)
All information will be formatted for the user locale based in the location of the browser or by GeoIP.

### Browser compatibility
The ABA English platform should work consistently across the following browsers:
* Safari
* Firefox
* Chrome
* Internet Explorer 9 (and above)


## Constraints
This section provides information about the constraints imposed on the development of the ABA English platform.


## Principles
This section provides information about the principles adopted for the development of the ABA English platform:

### Microservices
The ABA English platform will be composed by microservices. Microservices are modular, composable and fine-grained units that do one thing and one thing only. Each microservice must be built around business capabilities and independently deployable by fully automated deployment machinery.

### Twelve-Factor App
All our microservices must follow the [12factor](12factor.md)

### Automated testing
The ABA English code must be tested. Every microservice must provide his unit and integration tests. Our goal is to achieve 70% code coverage for automated tests.

### Static analysis tools
The ABA English code must pass rules defined in our code quality tool.

### Convention over Configuration
Microservices of ABA English platform will share common code and configuration, to avoid duplication we will use [Convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration) whenever it's possible.

Java microservices will use [Aba Boot](https://github.com/abaenglish/aba-boot) based on Spring Boot.

### Cloud Configuration Server
To facilitate the microservice deployment and the operations work, microservices will load properties from a backing service called [Spring Cloud Config](https://cloud.spring.io/spring-cloud-config/spring-cloud-config.html).

### Continuous integration, delivery and deployment
We want to be able to commit new features and bugfixes from develop branch to production environment through a pipeline that ensures the reliability of the code.


## Software Architecture
This section provides an overview of the ABA English software architecture.

### Context Diagram
![](../assets/imgs/software_architecture_context.png)

### Container Diagram
The following diagram shows a high-level microservices dependency tree. If you need to add new dependencies you can edit it [here](https://www.draw.io/#G0B5z6tqW0LamQNF8xTnBCUjNNU1k)

![](../assets/imgs/software_architecture_dependency_tree.png)

The following diagram shows a high-level communication between services and backing services.

![](../assets/imgs/software_architecture_container.png)


## Infrastructure Architecture
This section provides information about the infrastructure architecture of the ABA English platform.

### Live Environment
The live environment is hosted on [Amazon Web Services](https://aws.amazon.com/), in the production account (_abaenglish_), through a [Kubernetes cluster](http://kubernetes.io/). Microservices are executed inside the Kubernetes cluster and backing services (i.e.: MySQL, Redis, RabbitMQ) are executed outside the Kubernetes cluster.

#### Kubernetes cluster (AWS EC2)
__Masters__
* Number of instances: 3
* Instance type: m3.medium (CPU: 3 virtual cores, Memory: 3.75 GB RAM)
* Instance naming:
  * master-{region}-{disponibilityZone}.masters.pro.k8s.aba.solutions
* Key pair: production
* Security group: masters.pro.k8s.aba.solutions

__Nodes__
* Number of instances: variable according to needs (currently 2)
* Instance type: m3.medium (CPU: 3 virtual cores, Memory: 3.75 GB RAM)
* Instance naming:
  * nodes.pro.k8s.aba.solutions
  * nodes.pro.k8s.aba.solutions
* Key pair: production
* Security group: nodes.pro.k8s.aba.solutions

#### RabbitMQ (AWS EC2)
* Instance name: **rabbitmq-server-master**
* Instance id: i-0365d919e1cdca151
* Instance type: t2.micro (CPU: 1 virtual cores, Memory: 1 GB RAM, Volume: gp2 8 GB)
* Elastic IPs: 52.212.235.32
* Root volume type: General Purpose (SSD)
* Availability Zones: Any
* Key pair: production
* Security group: rabbitmq

* Instance name: **rabbitmq-server-slave**
* Instance id: i-04767e7f6c8ae4faf
* Instance type: t2.micro (CPU: 1 virtual cores, Memory: 1 GB RAM, Volume: gp2 8 GB)
* Elastic IPs: 52.212.179.151
* Root volume type: General Purpose (SSD)
* Availability Zones: Any
* Key pair: production
* Security group: rabbitmq

#### Redis
[TBD]

#### Config server (AWS Elastic Beanstalk)
__Application__
* Application: **cloud-config-server**
* Environment name: **cloud-config-server-prod**
* Environment id: e-m2mfvjurbh
* Environment URL: http://cloud-config-server-pro.2grcvq3qpm.eu-west-1.elasticbeanstalk.com/
* Environment tier: Docker (64bit Amazon Linux 2016.03 v2.1.3 running Docker 1.11.1)

__Instances__
* Instance type: t2.micro (CPU: 2 virtual cores, Memory: 4 GB RAM, Volume: gp2 8 GB)
* Root volume type: General Purpose (SSD)
* Availability Zones: Any
* Key pair: production
* Security group: awseb-e-m2mfvjurbh-stack-AWSEBSecurityGroup-F6LECVF0E40A

__Scaling__
* Environment type: Load balanced, auto scaling
* Number instances: 1 - 4
* Scale based on: Average network out
* Add instance when: > 6000000
* Remove instance when: < 2000000

__Configuration__
Using environment properties:
* CONFIG_SERVER_URI
* ENCRYPT_KEY
* ENVIRONMENT

#### Database server (AWS RDS)
* Instance id: **db-services-pro**
* Engine type: MySql
* Engine version: 5.7.11
* Endpoint: db-services-pro.cjvuilzfkqgf.eu-west-1.rds.amazonaws.com:3306
* Instance type: db.m3.medium (CPU: 3 virtual cores, Memory: 3.75 GB RAM)
* Storage: 100 GB
* Security group: RDS-Office (sg-a964f3ce)
* Multi Availability Zone: false

Has instance **db-services-pro-slave1** in replica mode.

#### Domains

* db-services.aba.solutions -> A (alias) db-services-pro.cjvuilzfkqgf.eu-west-1.rds.amazonaws.com
* db-services-pro.aba.solutions -> A (alias) db-services-pro.cjvuilzfkqgf.eu-west-1.rds.amazonaws.com

DNS Manager: [AWS Route 53](https://console.aws.amazon.com)

### Non-live environment
Aba English platform is a unique Kubernetes cluster to host all non-production environemnts. The actual environments are:
* dev (all code on develop branch will fit here)
* qa (all code from feature, bugfix and hotfix branches will fit on qa environment)

For these two environments, all backing services (RabbitMQ, Redis, Config server and Websocket) are included inside the cluster.

#### Kubernetes cluster (AWS EC2) for development
__Masters__
* Number of instances: 1
* Instance type: m3.medium (CPU: 3 virtual cores, Memory: 3.75 GB RAM)
* Instance naming:
  * i-0dc190ba379dd15fd (master-eu-west-1a.masters.k8s.aba.land)
* Key pair: devops
* Security group: masters.k8s.aba.land

__Nodes__
* Number of instances: variable according to needs (currently 2)
* Instance type: m3.large (CPU: 2 virtual cores, Memory: 7.5 GB RAM)
* Instance id:
  * nodes.k8s.aba.land
* Key pair: devops
* Security group: nodes.k8s.aba.land

__Environments__
Every environment in development cluster is group by namespace, the actual namespaces:
* dev (where the develop branch is deployed with every push)
* qa (where feature branches are deployed to  )


#### Database server (AWS RDS) for development
dev* Engine type: MySql
* Engine version: 5.7.11
* Endpoint: db-services-dev.cfbep2aydaua.eu-west-1.rds.amazonaws.com:3306
* Instance type: db.t2.micro (CPU: 2 virtual cores, Memory: 4 GB RAM)
* Storage: 2 GB
* Security group: default (sg-dedc19bb)
* Multi Availability Zone: false

#### Domains
* db-services-dev.aba.land -> A (alias) db-services-dev.cfbep2aydaua.eu-west-1.rds.amazonaws.com

DNS Manager: [AWS Route 53](https://console.aws.amazon.com)


## Deployment
This section provides information about the mapping between the [software architecture](#software-architecture) and the [infrastructure architecture](#infrastructure-architecture).

### Software
This section shows the stack of languages and technologies used by ABA English platform:
* Java 8 (Open JDK)
* Spring Boot 1.4.x
* ReactJS
* Node.js
* Nginx 1.10 (used for api gateway purpose)
* MySQL 5.7
* Redis 3.2
* RabbitMQ 3.6

### Building
To avoid the "it works on my machine" syndrome, plus to ensure that builds are clean and repeatable, all releases are built as docker images by a continuous integration server ([JenkinsCI](http://jenkins.aba.land:8080)).

This is the big picture of our continuous deployment:

![Continuous Integration](../assets/imgs/continuous_integration.png)

We use JenkinsCI [pipelines](https://jenkins.io/doc/book/pipeline/), so every microservice has at least a Jenkinsfile defining how is built.

To read more about our specific pipelines you can jump to [JenkinsCI Jobs and Jenkinsfile](#jenkinsci-jobs-and-jenkinsfile) section.

All java microservices use a [docker base image](https://github.com/rcruzper/openjdk) to avoid version issues.

### Deploying
All of the ABA English software is installed with ansible playbooks created by our team. Ansible playbooks can be found in our [GitHub account](https://github.com/abaenglish/devops-k8s-deploy). Also our continuous integration server allows the deployment through a [devops job](http://jenkins.aba.land:8080/job/Devops/job/Docker%20Deploy%20Kubernetes/). To execute this job you got to fill some data:
* service (i.e.: user-service, payment-funnel)
* image_version (docker image version)
* environment (i.e.: dev, qa)

### Configuration
The configuration served by [cloud config server](https://github.com/abaenglish/cloud-config-server) can be found [here](https://github.com/abaenglish/cloud-config-server-properties)


## Development

### Code repositories
ABA English platform is using [Github code repositories](https://github.com/abaenglish/).

__Front-end services__
* [Payment funnel](https://github.com/abaenglish/payment-funnel)

__Back-end services__
* [Websockets](https://github.com/abaenglish/websockets-to-mq)
* [Subscription service](https://github.com/abaenglish/backend-subscription-service)
* [User service](https://github.com/abaenglish/backend-user-service)
* [Plan service](https://github.com/abaenglish/backend-plan-service)
* [Paymentform service](https://github.com/abaenglish/backend-paymentform-service)

__Backing services__
* [API Gateway](https://github.com/abaenglish/backing-api-gateway)
* [Cloud Config Server](https://github.com/abaenglish/cloud-config-server)
* [Configuration properties](https://github.com/abaenglish/cloud-config-server-properties)

__Libraries__
* [Aba Boot](https://github.com/abaenglish/aba-boot)
* [External services with AbaWebapps](https://github.com/abaenglish/external-service-abawebapps)
* [External services with Paypal](https://github.com/abaenglish/external-service-paypal)
* [External services with Selligent](https://github.com/abaenglish/external-service-selligent)
* [External services with Zuora](https://github.com/abaenglish/external-service-zuora)
* [Zuora z-java](https://github.com/abaenglish/z-java)

### Environment setup

To setup a local environment for ABAEnglish platform we will use a cluster.

#### ABA English platform execution
To setup a local environment with ABA English software, we will use [minikube](https://github.com/kubernetes/minikube) and our [ansible playbooks](https://github.com/abaenglish/devops-k8s-deploy).

These are the requeriments:
- Update brew repository
    - brew update
- clone this repository
    - git clone git@github.com:abaenglish/devops-k8s-deploy.git
- awscli
    - brew install awscli
- Ansible
    - brew install ansible
- [minikube](https://github.com/kubernetes/minikube)
    - brew cask install minikube
- xhyve driver
    - https://github.com/kubernetes/minikube/blob/master/DRIVERS.md#kvm-driver
- kubectl
    - brew install kubectl
- AWS account with decrypt permissions (if you have configured AWS with abaland credentials, skip this step)
    - ask to imorato and you shall receive

To initialize a kubernetes cluster in our machine we will execute:
```shell
$ minikube start --vm-driver=xhyve
Starting local Kubernetes cluster...
Kubectl is now configured to use the cluster.

Open Minikube dashboard
$ minikube dashboard
```

After that we will have a kubernetes cluster running on our machine. Next thing we must do is execute the following command to load all services on the cluster:

```shell
$ ansible-playbook -i minikube site.yml --ask-become-pass
```

The execution of site.yml playbook will add an entry on /etc/hosts with the ip of the cluster named ```gateway.local```. Port 80 will be mapped directly to the api-gateway.

#### Nexus configuration
We are using Nexus OSS 3.x.x as Repository Manager to manage internal artifacts: maven, npm and docker images.

You can access to nexus via [web](http://nexus.aba.land:8081/) with a generic ```developer``` user and ```TtRB9xV9JcwgkwdP``` password or with your specific user.

##### Maven artifacts
You'll need to configure your computer to allow maven to upload and download artifacts. You can specify your user configuration in ```~/.m2/settings.xml``` adding the following lines:

```xml
<settings>
    <servers>
        <server>
            <id>nexus</id>
            <username>developer</username>
            <password>TtRB9xV9JcwgkwdP</password>
        </server>
        <server>
            <id>nexus-snapshots</id>
            <username>developer</username>
            <password>TtRB9xV9JcwgkwdP</password>
        </server>
        <server>
            <id>nexus-docker</id>
            <username>developer</username>
            <password>TtRB9xV9JcwgkwdP</password>
        </server>
    </servers>
</settings>
```

More information on how to configure maven: https://maven.apache.org/guides/mini/guide-configuring-maven.html

#### Docker images
You will need to login on the private registry created in nexus to be able to push and pull docker images with the following command:
```shell
$ docker login nexus.aba.land:5000
```

#### Npm packages

Nexus Repository Manager is currently used to store ABA's private npm pacakges. It can be accessed at https://nexus.aba.land:8443/

##### Prerequisites

To publish and consume our private npm packages, we first need to modify npm's config file found in the developer's home directory (~/.npmrc)

Publishing requires authentication. It can be configured by adding an _auth value to .npmrc. The value has to be generated by base64-encoding the string of username:password. You can create this encoded string with the command line call openssl e.g.: for the default admin user:

```
echo -n 'admin:admin123' | openssl base64
```

Other tools for the encoding are uuencode or, for Windows users, certutil. To use certutil on Windows you need to put the credentials to be encoded into a file:

```
admin:admin123
```

Then run:

```
c:\certutil /encode in.txt out.txt
```

After this the base64 encoded credentials can be found in between the begin and end certificate lines in the output file:

```
-----BEGIN CERTIFICATE-----
YWRtaW46YWRtaW4xMjM=
-----END CERTIFICATE-----
```
Once you have the encoded credentials the value as well as author information can then be added to the .npmrc file (IMPORTANT, the auth credentials specified here are fake, please generate the correct ones youself):

```
registry=https://nexus.aba.land:8443/repository/npm-all/
always-auth=true
_auth=YWRtaW46YWRtaW4xMjM
email=developer@abaenglish.com
```

##### Publishing the plugin

```
npm publish
```

Please remember to bump the plugin's version before publishing.

### How to code in ABA English
This section describes the best practices we must follow to make easy the way we show, read and share our code. Those practices apply to all repositories.

#### README.md
Every repository must have a file called README.md with information about the specific service:
* Information: Regarding the build status of the develop branch and a text describing the purpose of the service.
* Index: We use [doctoc](https://github.com/thlorenz/doctoc)
* Usage: Information about the usage of the service.
* Getting Started: A section defining how to be executed. Giving information if the service requires some of the microservices or backing services inside the minikube cluster and how to link to them.

#### CHANGELOG.md
Every repository must have a file called CHANGELOG.md to show the changes between versions:
* The versions will be ordered from newest to oldest.
* Every pull request from a feature o bugfix branch must have an entry on changelog file under Unreleased section.
* Unreleased section will change to version number before the release.
* The sections under a version will be: Added, Removed, Changed, Fixed

Example:

```markdown
# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased
### Added
- the best feature never seen.

## [1.2.0] - 2016-12-16
### Changed
- changed docker base image for microservices.

## [1.1.0] - 2016-11-23
### Added
- added new module (aba-boot-starter-cache) to activate cache.
```

Reference: [Keep a Changelog](http://keepachangelog.com/en/0.3.0/)

#### Commits
We follow some well-known strategies to push commits to the repos:

* Branch model: [Gitflow](http://nvie.com/posts/a-successful-git-branching-model/) as branching model
* Commit messages: We write commit messages based on [Git Commit Guidelines](https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md#-git-commit-guidelines) from AngularJS. To follow our guidelines you can jump to [commit rules](commits.md)

#### Pull requests
Every commit on develop branch has to be made through a pull request, these are the practices we follow to make a good pull request:
* Must close or solve a JIRA task/user story/bug.
* CHANGELOG.md has to be modified if code change.
* SonarQube has to validate the new code (coverage, code smell, bugs).
* Jenkins job has to return ok.
* We follow unique commit policy to accept pull request, that means that the pull request contains only one commit. If you pushed more commits in your branch you can [meld](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase-i) all in one commit with these commands on the feature/bugfix branch:
  * git rebase -i origin/develop
    (this command will initialize an interactive rebase regarding the remote target branch, origin/develop in this case)
  * An editor will be open, the commits that will be melt are shown in descending order, so only the first commit will be ```pick``` and the rest will be marked as ```squash```, after save and close the editor, a new open will be opened to rewrite the commit message of the new unique commit.

#### Versioning Code
We use [Semantic Versioning 2.0.0](http://semver.org/) to release our code.

#### Versioning Docker image
We need to create executables to be deployed in any environment. To accomplish this we will use Docker to deliver our apps.

We will set our docker tags matching gitflow branches as follows:
- develop -> ```x.y.z-SNAPSHOT-abbrev```
- master -> ```x.y.z``` and ```latest```
- bugfix and feature branches -> ```bugfix_????``` or ```feature_????```

#### Github repository settings
[TBD]

#### JenkinsCI jobs and Jenkinsfile
This section provides information about our continuous integration and deployment.

We create three pipelines inside a folder for every repo/project:
- Multibranch pipeline job: Creates a set of pipelines according to detected branches in one repository. You can set a regular expression to avoid branches.
- Pipeline job (relase): Job to create a release.
- Pipeline job (hotfix): Job to create a hotfix.

We can use [User Service](http://jenkins.aba.land:8080/job/Backend/job/User%20Service/) as a template for other projects. Also to share method between pipelines we use this [repository](https://github.com/abaenglish/jenkins-pipeline-library)


## Operation & Support

### Services
[TBD]

### Crons
[TBD]

### Monitorization
* *[AWS Cloudwatch](https://eu-west-1.console.aws.amazon.com/cloudwatch/)*
No alarms are set for the AWS Elastic Beanstalk nor AWS RDS database.

* *[New Relic APM](https://rpm.newrelic.com/accounts/1268965/applications)*
Payment funnel
https://rpm.newrelic.com/accounts/1268965/applications/29530700
User service
https://rpm.newrelic.com/accounts/1268965/applications/29527479
Subscription service
https://rpm.newrelic.com/accounts/1268965/applications/29527678
Websocket to mq
https://rpm.newrelic.com/accounts/1268965/applications/29530081
Cloud configuration server
https://rpm.newrelic.com/accounts/1268965/applications/29526484

There is also for QA environment.

Checks the availability of resources in the servers. Notification is sent to Slack channel #logs and itsystems@abaenglish.com receives an email.

* *[New Relic Browser](https://rpm.newrelic.com/accounts/1268965/browser/17846093/v2_page_views)*
  Checks the resources used when browsing through web pages. No alerts are set and therefore no notifications are sent.

* *[New Relic Synthetics](https://synthetics.newrelic.com/accounts/1268965/synthetics)*
Payment funnel
https://synthetics.newrelic.com/accounts/1268965/monitors/75f8ff24-1203-41cf-ba5a-57b59b2f56e2
https://synthetics.newrelic.com/accounts/1268965/monitors/fa49ddc3-6ea6-4dc3-86e3-e4195c1fb923
Subscription service
https://synthetics.newrelic.com/accounts/1268965/monitors/a25cf2b0-c9a9-4e06-8627-b9ae8a380a32
https://synthetics.newrelic.com/accounts/1268965/monitors/d4b5b8b4-7596-4eb6-b9a8-b6816d616c0b
Websocket to mq
https://synthetics.newrelic.com/accounts/1268965/monitors/a9272f61-15dd-4457-853d-6bc69fc5aa56
Cloud configuration server
https://synthetics.newrelic.com/accounts/1268965/monitors/6555d234-a361-495b-bf49-08deef8fc42f

There is also for QA environment.

Ping checks to the blog from different locations. Notification is sent to Slack channel #logs and itsystems@abaenglish.com receives an email.

### Backups
Snapshots are made to the database every day by AWS RDS. They can be accessed in AWS RDS Snapshot console](https://eu-west-1.console.aws.amazon.com/rds/home?region=eu-west-1#db-snapshots:).

### Contingency
There is no contingency policy in case of service disruption.

## TODO
- [ ] Context diagram
- [ ] Live environment Redis description
- [ ] Npm packages
- [ ] Operation & support
- [ ] HA Proxy
