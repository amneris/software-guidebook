# ABA Platform Version 4 (aka AbaWebapps)

| Notice       |
| :------------- |
| Until all ABA English platform v4 (aka AbaWebapps) ends its useful life, this v5 guidebook must be read along with v4 guidebook. |

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Context](#context)
  - [Users](#users)
  - [External Systems](#external-systems)
- [Functional Overview](#functional-overview)
  - [Modules](#modules)
- [Quality Attributes](#quality-attributes)
  - [Performance](#performance)
  - [Scalability](#scalability)
  - [Security](#security)
  - [Availability](#availability)
  - [Internationalisation (i18n)](#internationalisation-i18n)
  - [Localisation (L10n)](#localisation-l10n)
  - [Compatibility](#compatibility)
- [Constraints](#constraints)
- [Principles](#principles)
  - [Twelve-Factor App](#twelve-factor-app)
  - [Automated testing](#automated-testing)
  - [Static analysis tools](#static-analysis-tools)
  - [Convention over Configuration](#convention-over-configuration)
  - [Continuous integration, delivery and deployment](#continuous-integration-delivery-and-deployment)
- [Software Architecture](#software-architecture)
  - [Components Diagram](#components-diagram)
  - [Container Diagram](#container-diagram)
- [Infrastructure Architecture](#infrastructure-architecture)
  - [Live Environment](#live-environment)
- [Load Balancer (1)](#load-balancer-1)
- [Deployment](#deployment)
  - [Software](#software)
  - [Building](#building)
  - [Deploying](#deploying)
  - [Configuration](#configuration)
- [Development](#development)
  - [Environment setup](#environment-setup)
- [Operation & Support](#operation--support)
  - [Starting MySQL](#starting-mysql)
  - [Starting the Web Server](#starting-the-web-server)
  - [Crons](#crons)
  - [Monitorization](#monitorization)
  - [Backups](#backups)
- [TODO](#todo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

This software guidebook provides an overview of the ABA English software platforms v4 (aka AbaWebapps). It includes a summary of the following:

1. The requirements, constraints and principles behind the platform.
2. The software architecture, including the high-level technology choices and structure of
the software.
3. The infrastructure architecture and how the software is deployed.
4. Operational and support aspects of all the systems involved.

## Context

ABA English software platform v4 is responsible of the delivery of ABA English's course and its management, for B2C and B2B customers.

Here’s a context diagram that provides a visual summary of this:

[TBD]

### Users

The platform provides services for internal (ABA employees) and external (mainly students) users.

There are three types of external users:

1. Anonymous: anybody with a web browser can access content on the public website and the blogs.
2. Free: registered users can sign-in to the platform and access one tier of content for free (access to premium features is restricted, obviously :)
3. Premium: Have access to the whole course and enjoy all the ABA Films and Video Classes. All our course levels are available to you.

ABA employeers are distributed in different roles:

1. Teacher 
2. Support Agent
3. Finance
4. Data Analist
5. Revenue & Product manager
6. Media buyers (Acquisition manager)
7. B2B account managers
8. Admins

### External Systems

There are many external systems that the platform integrates with. These are represented by dashed grey boxes on the context diagram.

* Adjust
* CoolaData
* Facebook
* Google Analytics
* Google Tag Manager
* Payments Gateways (Adyen, Sermepa, PayPal, Stripe, AllPago)
* Mobile apps stores (iTunes Connect, Google PlayStore, Google Payments)
* LinkedIn
* Mixpanel
* Selligent
* Upsight
* Vimeo, Vzaar
* Zendesk


## Functional Overview
[TBD]

### Modules

[TBD]

#### Payment funnel

Current payment workflow has five steps:

**Step 1. Plan/Product selection by customer**
`https://campus.abaenglish.com/es/payments/payment/?promocode=______`

All available plans are shown in the plans page, with their prices and discounts. The customer can have arrived to the page directly (by a link) or from Campus.

If the customer has a __promocode__, he introduces it in the promocode field and the page reloads passing the promocode as a variable in the URL => **Step 1**

The customer selects the plan to subscribe => **Step 2**

**Step 2. Payment method selection and payment data**
`https://campus.abaenglish.com/es/payments/paymentmethod`

The different payment methods are shown to the customer, depending in their country (table `aba_b2c.cpuntry_paymethod_paysupplier`). The payment methods available are stored in table `aba_b2c.pay_methods` (Credit card, Paypal, Boleto, Adyen).

In case of payment by **credit card** the customer has to fill the form with the credit card data and its redirected to La Caixa => **Step 3'**

In case of payment by **Paypal** the customer is redirected to Paypal => **Step 3''**

In case of payment using **alternative methods** the customer must choose the alternative method (they appear as buttons with images of the logos of the payment method) and then is redirected to the corresponding payment gateway. For all the payment methods coming from **Adyen** it is first done a query to Adyen's API with the currency and customer's country to obtain the different gateays configured in Adyen's manager => **Step 3'''**

**Step 3. Payment gateways**

* **Step 3'** La Caixa payment gateway

Payment is processed with the previous introduced data in the form. Redirected to the confirmation OK page ( => **Step 4**) or KO page ( => **Step 5**)

* **Step 3''** Paypal payment gateway

After the login in Paypal, the payment is processed. Redirected to the confirmation OK page ( => **Step 4**) or KO page ( => **Step 5**)

* **Step 3'''** Adyen payment gateway

According to the gateway selected in Adyen, it could demand the payment method and after the payment data (depends in the country).

Redirected to the confirmation OK page ( => **Step 4**) or KO page ( => **Step 5**)

**Step 4. Payment OK page**

If the payment has been correctly processed, then the confirmation page is shown, showing a summary of the payment and a link to the campus.

**Step 5. Payment KO page**

If there has been a cancellation or an error during the payment process, then the error page is shown (is the same as the confirmation page). 

There is also a **PaymentPlus** process for Premium users renewing their subscription. Access is done by a link from an email sent by Marketing.


#### Course

Course content and study material (Levels, Units and Sections) with an API for mobile.

##### Emailing & Promotions

Mailing and other marketing promotions to engage user and convert them to Premium.

##### Payments & Subscriptions

* Product Catalog Management
* Pricing Strategy Management
* Automated Renewals System
* Upgrades/Downgrades with proration and billing days

##### Internal messaging

Multi-platform messaging service and internal communication, primarily between teachers and student.

##### Follow-up & Auditing

Monitoring of the minimum data of the user's progress, required to allow him to continue in his learning itinerary.

##### Reporting

Revenue, course and other business reports.

##### Back-office (Intranet)

Main dashboard that integrates with all the backoffices of all other modules. Backoffice of system configuration and feature toggles.

##### ABA for Business (Corporate)

Management of the B2B corporate customers and its students.


## Quality Attributes
[TBD]

This section provides information about the desired quality attributes (non-functional requirements) of the ABA English platform.

### Performance
We expect to serve content in a time no superior to five seconds no matter the number of users connected.

### Scalability
The ABA English platform has to be scalable both vertically and horizontally.

### Security
Our platform will use methods to secure the access of our users to all our services.

### Availability
The ABA English software platform must be always up and running. To accomplish that our platform will perform [Blue-green deployments](http://martinfowler.com/bliki/BlueGreenDeployment.html).

### Internationalisation (i18n)
All user interface text will be presented in user's language based on the browser or by GeoIP.

### Localisation (L10n)
All information will be formatted for the user locale based in the location of the browser or by GeoIP.

### Compatibility
The ABA English platform should work consistently across the following browsers:
* Safari
* Firefox
* Chrome
* Internet Explorer 9 (and above)


## Constraints
This section provides information about the constraints imposed on the development of the ABA English platform.


## Principles
This section provides information about the principles adopted for the development of the ABA English platform:

### Twelve-Factor App
All our microservices must follow the [12factor](12factor.md)

### Automated testing
All ABA English code must be tested, providing unit and integration tests. Functional tests, when UI exists, is also required. Our goal is to achieve 70% code coverage for automated tests.

### Static analysis tools
All ABA English code must pass rules defined in our code quality tool.

### Convention over Configuration
All ABA English platform will share common code and configuration, to avoid duplication we will use [Convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration) whenever it's possible.

### Continuous integration, delivery and deployment
We want to be able to commit new features and bugfixes from develop branch to production environment through a pipeline that ensures the reliability of the code.


## Software Architecture

### Components Diagram
The following diagram shows the components that make up the ABA English platform.
[TBD]

### Container Diagram
The following diagram shows the logical containers that make up the ABA English platform system. The diagram does not represent the physical number and location of containers (please see the [Infrastructure](#infraestructure) and [Deployment](#deployment) sections for this information).
[TBD]

## Infrastructure Architecture
This section provides information about the infrastructure architecture of the ABA English platform.

### Live Environment
The live environment is hosted on [Nexica](https://www.nexica.com/), consisting in one load balancer distributing traffic to five web servers and a database server.

## Load Balancer (1)
IP: 212.92.50.150
Distributes traffic coming to campus.abaenglish.com, api.abaenglish.com, intranet.abaenglish.com, corporate.abaenglish.com

#### Web servers (5)
* campusweb01 (IP: 217.13.118.208, internal: 17.16.1.211)
* campusweb02 (IP: 217.13.118.206, internal: 17.16.1.212)
* campusweb03 (IP: 217.13.118.209, internal: 17.16.1.213)
* campusweb04 (IP: 217.13.118.210, internal: 17.16.1.214)
* campusweb05 (IP: 217.13.118.211, internal: 17.16.1.215)

__Features__ 
* CPU: 2 cores
* Memory: 4 GB RAM
* HDD: 16 GB
* Operating System: CentOS Linux release 7.1.1503 (Core)
* Web Server: Apache HTTP Server 2.4.6 (Unix)

__SSL certificate__
* Type: Comodo PremiumSSL Wildcard
* Domains: *.abaenglish.com
* Issued: 05/03/2016
* Expires: 05/03/2019

#### Database server (2)
* dbcampus (IP: internal 172.16.1.201)

Has instance **dbcampusslave** as slave in replica mode (IP: internal 172.16.1.69).

__Features__ 
* CPU: 8 cores
* Memory: 64 GB RAM
* HDD: 600 GB
* Percona Server 5.6.26-74.0-log

#### Domains
* campus.abaenglish.com -> A 212.92.50.150
* api.abaenglish.com -> A 212.92.50.150
* intranet.abaenglish.com -> A 212.92.50.150
* corporate.abaenglish.com -> A 212.92.50.150

DNS manager: [Nexica](https://cloudmanager.nexica.com/)

### Non-live environment
ABA English platform uses Vagrant to create the development environment. The actual environments are:
* dev (all code on develop branch will fit here)
* integration (all code on integration branch will fit here)
* qa (hosts specific version required for QA)

The integration environment is hosted on [Amazon Web Services](https://aws.amazon.com/), in the development account (_abaland_), consisting in an Elastic Beanstalk application (load balancer and web servers) and a RDS database per environment. 

#### Development environment
__Application__
* Application: **abawebapps**
* Environment name: **abawebapps-dev**
* Environment id: e-fp3ysmhyqe
* Environment URL: https://abawebapps-dev.elasticbeanstalk.com/
* Environment tier: Web Server (64bit Amazon Linux 2016.09 v2.0.4 running PHP 5.5)

__Instances__
* Instance type: t2.micro (CPU: 1 virtual cores, Memory: 1 GB RAM, Volume: gp2 8 GB)
* Root volume type: General Purpose (SSD)
* Availability Zones: Any
* Key pair: production
* Security group: awseb-e-fp3ysmhyqe-stack-AWSEBSecurityGroup-TA75B5X0Q5C4
* Environment variables:
  * ABA_SERVERNAME
  * ABAWEBAPPS_ENV
  * CAMPUS_URL
  * Extranet_URL
  * INTRANET_URL
  * DB_HOSTNAME
  * DB_USERNAME
  * DB_PASSWORD
  * DBSLAVE_HOSTNAME  
  * DBSLAVE_USERNAME
  * DBSLAVE_PASSWORD
  * DB3_HOSTNAME
  * DB3_USERNAME
  * DB3_PASSWORD
  * TRANSACTION_DB_HOST
  * TRANSACTION_DB_USER
  * TRANSACTION_DB_NAME
  * TRANSACTION_DB_PASS
  * GLOSSARY_DB_HOST
  * GLOSSARY_DB_USER
  * GLOSSARY_DB_PASS
  * AWS_SECRET_ACCESS_KEY
  * _AWS_BUCKET
  * AWS_URL_DOMAIN
  * AWS_BUCKET
  * AWS_ACCESS_KEY_ID
  * CORP_MAGIC_KEY
  * _SHAREDFOLDER
  * YII_DEBUG
  * COOLADATA_SDK_API_KEY
  
__Scaling__
* Environment type: Load balanced, auto scaling
* Number instances: 1 - 4
* Scale based on: Average network out
* Add instance when: > 6000000
* Remove instance when: < 2000000
 
__SSL certificate__
* Type: DonDominio Wildcard SSL
* Domains: *.aba.land
* Issued: 15/06/2016
* Expires: 15/06/2019

__Database__
* Instance id: **dbdev**
* Engine type: MySql
* Engine version: 5.6.22
* Endpoint: dbdev.cfbep2aydaua.eu-west-1.rds.amazonaws.com:3306
* Instance type: db.m3.medium (CPU: 3 virtual cores, Memory: 3.75 GB RAM)
* Storage: 300 GB
* Security group: RDS-dbdev (sg-e59b4c81)
* Multi Availability Zone: false

__Domains__
campus-dev.aba.land -> CNAME abawebapps-dev.eu-west-1.elasticbeanstalk.com
campus.dev.aba.land -> CNAME abawebapps-dev.eu-west-1.elasticbeanstalk.com

#### Integration environment
__Application__
* Application: **abawebapps**
* Environment name: **abawebapps-int**
* Environment id: e-7pk7pt362m
* Environment URL: https://abawebapps-int.elasticbeanstalk.com/
* Environment tier: Web Server (64bit Amazon Linux 2016.09 v2.2.0 running PHP 5.5)

__Instances__
* Instance type: t2.micro (CPU: 1 virtual cores, Memory: 1 GB RAM, Volume: gp2 8 GB)
* Root volume type: General Purpose (SSD)
* Availability Zones: Any
* Key pair: production
* Security group: awseb-e-7pk7pt362m-stack-AWSEBSecurityGroup-1HS3QAV20UFN1
* Environment variables:
  * ABA_SERVERNAME
  * ABAWEBAPPS_ENV
  * CAMPUS_URL
  * Extranet_URL
  * INTRANET_URL
  * DB_HOSTNAME
  * DB_USERNAME
  * DB_PASSWORD
  * DBSLAVE_HOSTNAME  
  * DBSLAVE_USERNAME
  * DBSLAVE_PASSWORD
  * DB3_HOSTNAME
  * DB3_USERNAME
  * DB3_PASSWORD
  * TRANSACTION_DB_HOST
  * TRANSACTION_DB_USER
  * TRANSACTION_DB_NAME
  * TRANSACTION_DB_PASS
  * GLOSSARY_DB_HOST
  * GLOSSARY_DB_USER
  * GLOSSARY_DB_PASS
  * AWS_SECRET_ACCESS_KEY
  * _AWS_BUCKET
  * AWS_URL_DOMAIN
  * AWS_BUCKET
  * AWS_ACCESS_KEY_ID
  * CORP_MAGIC_KEY
  * _SHAREDFOLDER
  * YII_DEBUG
  * COOLADATA_SDK_API_KEY
  
__Scaling__
* Environment type: Load balanced, auto scaling
* Number instances: 1 - 4
* Scale based on: Average network out
* Add instance when: > 6000000
* Remove instance when: < 2000000
 
__SSL certificate__
* Type: DonDominio Wildcard SSL
* Domains: *.aba.land
* Issued: 15/06/2016
* Expires: 15/06/2019

__Database__
* Instance id: **dbint**
* Engine type: MySql
* Engine version: 5.6.22
* Endpoint: dbint.cfbep2aydaua.eu-west-1.rds.amazonaws.com:3306
* Instance type: db.m3.medium (CPU: 3 virtual cores, Memory: 3.75 GB RAM)
* Storage: 300 GB
* Security group: default (sg-dedc19bb)
* Multi Availability Zone: false

__Domains__
campus-int.aba.land -> A (alias) dualstack.awseb-e-7-awsebloa-1e3fu25gljorj-727066147.eu-west-1.elb.amazonaws.com.
campus.int.aba.land -> A (alias) dualstack.awseb-e-7-awsebloa-1e3fu25gljorj-727066147.eu-west-1.elb.amazonaws.com.




#### QA environment
__Application__
* Application: **abawebapps**
* Environment name: **abawebapps-qa**
* Environment id: e-eyytvvzu9m
* Environment URL: https://abawebapps-qa.elasticbeanstalk.com/
* Environment tier: Web Server (64bit Amazon Linux 2016.09 v2.1.6 running PHP 5.5)

__Instances__
* Instance type: t2.micro (CPU: 1 virtual cores, Memory: 1 GB RAM, Volume: gp2 8 GB)
* Root volume type: General Purpose (SSD)
* Availability Zones: Any
* Key pair: production
* Security group: awseb-e-eyytvvzu9m-stack-AWSEBSecurityGroup-UA68SZ87FBBN
* Environment variables:
  * ABA_SERVERNAME
  * ABAWEBAPPS_ENV
  * CAMPUS_URL
  * Extranet_URL
  * INTRANET_URL
  * DB_HOSTNAME
  * DB_USERNAME
  * DB_PASSWORD
  * DBSLAVE_HOSTNAME  
  * DBSLAVE_USERNAME
  * DBSLAVE_PASSWORD
  * DB3_HOSTNAME
  * DB3_USERNAME
  * DB3_PASSWORD
  * TRANSACTION_DB_HOST
  * TRANSACTION_DB_USER
  * TRANSACTION_DB_NAME
  * TRANSACTION_DB_PASS
  * GLOSSARY_DB_HOST
  * GLOSSARY_DB_USER
  * GLOSSARY_DB_PASS
  * AWS_SECRET_ACCESS_KEY
  * _AWS_BUCKET
  * AWS_URL_DOMAIN
  * AWS_BUCKET
  * AWS_ACCESS_KEY_ID
  * CORP_MAGIC_KEY
  * _SHAREDFOLDER
  * YII_DEBUG
  * COOLADATA_SDK_API_KEY
  
__Scaling__
* Environment type: Load balanced, auto scaling
* Number instances: 1 - 4
* Scale based on: Average network out
* Add instance when: > 6000000
* Remove instance when: < 2000000
 
__SSL certificate__
* Type: DonDominio Wildcard SSL
* Domains: *.aba.land
* Issued: 15/06/2016
* Expires: 15/06/2019

__Database__
* Instance id: **dbqa-migrated**
* Engine type: MySql
* Engine version: 5.6.22
* Endpoint: dbqa-migrated.cfbep2aydaua.eu-west-1.rds.amazonaws.com:3306
* Instance type: db.m3.medium (CPU: 3 virtual cores, Memory: 3.75 GB RAM)
* Storage: 300 GB
* Security group: default (sg-dedc19bb)
* Multi Availability Zone: false

__Domains__
campus-qa.aba.land -> CNAME abawebapps-qa.eu-west-1.elasticbeanstalk.com
campus.qa.aba.land -> CNAME abawebapps-qa.eu-west-1.elasticbeanstalk.com

## Deployment
This section provides information about the mapping between the [software architecture](#software-architecture) and the [infrastructure architecture](#infrastructure-architecture).

### Software
This section shows the stack of languages and technologies used by ABA English website:
* PHP: 5.5
* Web Server: Apache HTTP Server 2.4 (Unix)
* MySql: 5.6

### Building
There is only building strategy for ABA English platform applied to integration environment. 

We use a continuous integration server ([JenkinsCI](http://jenkins.aba.land:8080)), that creates a job when a commit is pulled to the Git integration branch, deploying the source code into an AWS Elastic Beanstalk.

### Deploying

#### Cattle servers campusweb*
Release must be done in all servers of AbaWebapps (currently campusweb01, campusweb02, campusweb03, campusweb04, campusweb05).

We are currently using an [Ansible playbook](https://github.com/abaenglish/config-manager) to do all the deployments in cattle servers.

##### Preparation of the release
Previous to the deployment, the ABAWebapps release version must be set in ABAWebapps code, in master branch. More exactly in /common/protected/config/version and in /composer.json in `version` key. 

Bear in mind that if the abawebapps release is related to a course release, the new course version must be set in /composer.json, in the keys `repositories.package.version`, `repositories.package.dist.url` and in `require-dev.abaenglish/course`.

Run `composer update` in order to finish the release preparation, and don't forget to commit and push the code.

##### Release
Release must be done in Logger server.

1. Connect to the server by SSH using user aba and current IP is 172.16.1.18 (remember VPN to Nexica must be enabled)
2. `cd config manager`
3. `ansible-playbook deploy/deploy.yml -i deploy/hosts/prod -e "version=X.Y.Z"` (where X.Y.Z is the version to release)
It might take a while while doing all the actions
4. Check webapps in the server: campus-next.abaenglish.com, extranet-next.abaenglish.com, intranet-next.abaenglish.com, api-next.abaenglish.com (if you have configured your hosts file to point to any campusweb* server
If everything is OK, continue. If NOK then rollback

Curently, there is an error prompted in the screen when trying to delete the older release folders in the server. It rather uncomfortable to see all those red alerts, but deployment has been done correctly. There is an [issue](https://abaenglish.atlassian.net/browse/INO-37)) to fix this in the future.

In case deployment is required in the crons server:
1. Connect to the crons server by SSH (`ssh -i "production.pem" ec2-user@ec2-52-212-246-222.eu-west-1.compute.amazonaws.com`)
2. `su rundeck ` with rundeck password
3. `cd /var/app/abawebapps `
4. `git fetch && git checkout X.Y.Z ` (where X.Y.Z is the version to release)

In case changes have to be done in the database:
1. Connect to the database in host 172.16.1.201 with user (remebber VPN to Nexica must be enabled).
2. Execute the database delta in an SQL console.

##### Rollback

R1. `ansible-playbook deploy/rollback.yml -i deploy/hosts/prod`
R2. Check webapps in the server: campus.abaenglish.com, extranet.abaenglish.com, intranet.abaenglish.com, api.abaenglish.com

In case they were changes in the database, remember to rollback them also.

#### Pet servers web* (legacy)

##### Preparation of the release
Follow the same instructions as in pet servers.

##### Release
Release must be done in all servers of ABAWebapps (currently web3,web4,web6,web7,web8), but try it first in only one of them and test (normally as campus-next).

1. Connect to the server by SSH 
2. `cd /var/app/abawebapps-next`
3. `git fetch`
4. `git checkout X.Y.Z` (where X.Y.Z is the version to release)
5. `./composer.phar install` (It might take a while and requires plenty of server resources so it must be done only in one server at a time)
6. Check webapps in the server: campus-next.abaenglish.com, extranet-next.abaenglish.com, intranet-next.abaenglish.com, api-next.abaenglish.com
If everything is OK, continue. If NOK then rollback
7. `cd /var/app/webapps`
8. `service httpd stop` 
9. `git fetch`
10. `git checkout X.Y.Z` (where X.Y.Z is the version to release)
11. `./composer.phar install`
12. `service httpd start`
13. Check webapps in the server: campus.abaenglish.com, extranet.abaenglish.com, intranet.abaenglish.com, api.abaenglish.com

In case changes have to be done in the database:
1. Connect to the database in host 172.16.1.201 with user (remebber VPN to Nexica must be enabled).
2. Execute the database delta in an SQL console.

##### Rollback

R1. `git checkout A.B.C` (where A.B.C is the previous production version). Bear in mind always to know which version is currently in production

In case they were changes in the database, remember to rollback them also.

### Configuration
The configuration file for the different hosts in the same web server can be found in the /etc/httpd/conf/sites, one config file for each subdomain:
* 80-campus.abaenglish.com.conf
* 80-intranet.abaenglish.com.conf
* 80-api.abaenglish.com.conf
* 80-corporate.abaenglish.com.conf
* 80-trx.abaenglish.com.conf

Part of the configuration is also set by database, using database table `aba_b2c`.`config` and environmental variables.

## Development

### Code repositories
ABA English platform is using [Github code repositories](https://github.com/abaenglish/).

* [AbaWebapps](https://github.com/abaenglishabawebapps)
* [Course](https://github.com/abaenglishcourse)
* [Transactions](https://github.com/abaenglishtransactions)
* [Video Player](https://github.com/abaenglishabaplayer)

__DevOps__
* [Deployment playbook]https://github.com/abaenglishconfig-manager

### Environment setup
To setup a local environment for ABAEnglish website we will use a virtual machine using Vagrant. We are currently using a [VagranFile](https://bitbucket.org/abaenglish/vagrant-abawebapps-ansible) that uses Ansible playbooks, but the [legacy VagrantFile](https://bitbucket.org/abaenglish/vagrant-abawebapps) still works, although its use it not recommended.

__Requirements__
* Vagrant    >= 1.8.1
* Virtualbox >= 5.0
* Python >= 2.6
* Ansible >= 2

1. Install Ansible
    - `brew install ansible`
2. Install vbguest plugin (this plugin is for shared folders)
    - `sudo vagrant plugin install vagrant-vbguest`
3. Clone AbaWebapps repository
    - `git clone git@github.com:abaenglish/abawebapps.git`
4. Install composer locally:
    - `cd abawebapps`
    - `curl -sS https://getcomposer.org/installer | php`
5. Install Composer dependencies:
    - `./composer.phar install`
6. Setup /etc/hosts:
    - [Check the hosts configuration](https://github.com/abaenglishabawebappsdocs/hosts.md)
    - Add new Ansible playbook host configuration: 
`sed -i "192.168.50.2 campus.abaenglish.local intranet.abaenglish.local corporate.abaenglish.local api.abaenglish.local" /etc/hosts`
8. a) Mock the dependencies: the Vagrant setup requires 3 other projects (course, transactions and mylittleapps) but if you are not going to actively develop in those projects you can just mock the dependencies creating empty folders for the three of them
    - `mkdir course transactions mylittleapps`
8. b) Developing dependencies: course, transactions and mylittleapps are the 3 project dependencies required by AbaWebapps. To active develope those projects along with AbaWebapps you must install them:
    - `mkdir mylittleapps`
    - `git clone git@github.com:abaenglish/transactions.git`
    - `git clone git@github.com:abaenglish/course.git`
    - `cd campus/public/themes/ABAenglish/media ln -s ../../../../course ./course`
9. Clone AbaWebapps Vagrant repository in a folder outside AbaWebapps
    - `git clone git@bitbucket.org:abaenglish/vagrant-abawebapps-ansible.git`
10. Load the virtual machine:
    - `cd vagrant-abawebapps-ansible && vagrant up` 

Code will be modified in `/abawebapps` folder and you could check the results in a browser at `https://campus.abaenglish.local`, `https://intranet.abaenglish.local` and `corporate.abaenglish.local`.
 
Course will be modified in `/course`folder. Remember that course is related with AbaWebapps as a Composer dependency so changes in Course must be updated by Composer `composer update abaenglish/course`   
 
`/abawebapps/mylittleapps` folder is a playground place where to test things outside the main apps of AbaWwebapps (campus, intranet, corporate, api).

## Operation & Support

### Starting MySQL
MySQL is installed as a service in the database server, and should be running after a server restart. You can check this by using the following command:
`sudo netstat -tap | grep mysql`

If you need to restart MySQL, you can use the following command:
`sudo service mysql restart`

### Starting the Web Server
Apache HTTP Server is installed as a service in the web servers, and should be running after a server restart. You can check this by using the following command:
`sudo netstat -tap | grep httpd`

If you need to restart Apache, you can use the following command:
`sudo service httpd restart`

### MyDBR
To restart MyDBR server, due to high delay with master, you can use the following commands:
* `ssh mydbr`
* `mysql -u root -p`
* `SHOW SLAVE STATUS\G`
* `STOP SLAVE`
* `START SLAVE`
* `SHOW SLAVE STATUS\G` (check _Seconds_Behind_Master_ is reducing its value)

### Crons
AbaWebapps requires crons for its correct functionality. All crons are managed by [Rundeck](http://crons.aba.land:4440/).

List of crons used:
[TBD]

### Monitorization
* *[Nexica Icinga](https://monitoritzacio.nexica.com/)*
Checks the availability of resources in the servers. Can be displayed in the [IT Dashboard](http://dashboards.aba.land//it) and itsystems@abaenglish.com receives an email.

* *[New Relic APM](https://rpm.newrelic.com/accounts/1268965/applications/19471376)*

* *[New Relic Browser](https://rpm.newrelic.com/accounts/1268965/browser/19471376#/?percentile=50&_k=f0vrix)*
  Checks the resources used when browsing through web pages. No alerts are set and therefore no notifications are sent.

* *[New Relic Synthetics](https://synthetics.newrelic.com/accounts/1268965/monitors?filters=Monitoring:Campus)*
Ping checks to the website from different locations. Notification is sent to Slack channel #logs and itsystems@abaenglish.com receives an email.

### Backups
Backups are made by our IT Provider Nexica, in a daily basis for both
They can be accessed in _______ [TBD]

## TODO
- [ ] Context diagram
- [ ] trx.abaenglish.com
- [ ] callbacks.abaenglish.com
- [ ] List of crons
- [ ] Backups
- [ ] Assets management