## Quality Assurance 

This section provides an overview of the proccess of the Quality Software of the new apps, projects of ABA English 

The idea is to ensure that the product is what is wanted, and is of a required quality. Quality software is part of the project plan, not something that is thought about after, especially testing early on which can reduce the risk of a project. Developers should be encouraged to demo their work early and try to get the business to test also. When you test, this allows you to create a feedback loop, and promotes communication on a project.


We start with QA procces in the company with the implemetation of the Version 5 (micro-services oriented architecture) ABA Platform.    
In version 4 (aka AbaWebapps) and Mobile Projects  we are added test scenaries  just in case of the new features.  We hope that with the the time the old features could be add as a regression test.

We are working with test automation framework with a modular architecture to ensure that our tests are involved in the developer processes.
* https://github.com/abaenglish/qa-web-automation-tool
* https://github.com/abaenglish/qa-mobile-automation-tool

# Objetives for test automation

* earlier time to market
* reduce cost of testing 
* consistend repeatible testing
* run test unattendend
* find regression bugs
* run tests more offen

After the first-phase implementation in version 5 the followong objetives were added

* better quality software
* test more software


We believe that releasing new versions of a product should be routine and it encourages teams  to be constantly adding new features to its product, no matter how small or incomplete they are. Testing should be done throughout the lifecycle of the product – and focus should always be on delivering maximum business benefit. 

http://jenkins.aba.land:8080/job/QA/

We believe in colaborative testing, Effective and productive involves the input and collaboration of all people involved; all stakeholders. This will increase the productivity of the test, fix and reset cycle. This is inline with the Agile concept of collaboration – any one who can test something should! This usually means the business owners testing the features they requested.


