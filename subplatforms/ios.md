# ABA English iOS app

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Context](#context)
  - [Users](#users)
- [Functional Overview](#functional-overview)
  - [Modules](#modules)
- [Quality Attributes](#quality-attributes)
- [Constraints](#constraints)
- [Principles](#principles)
- [Software Architecture](#software-architecture)
  - [Components Diagram](#components-diagram)
  - [Container Diagram](#container-diagram)
- [Infrastructure Architecture](#infrastructure-architecture)
  - [Live Environment](#live-environment)
- [Deployment](#deployment)
  - [Software](#software)
  - [Building](#building)
  - [Deploying](#deploying)
  - [Configuration](#configuration)
- [Development](#development)
  - [Environment setup](#environment-setup)
- [Operation & Support](#operation--support)
  - [Starting Services](#starting-services)
  - [Crons](#crons)
  - [Monitorization](#monitorization)
  - [Backups](#backups)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction
[TBD]


## Context
[TBD]

### Users
[TBD]


## Functional Overview
[TBD]

### Modules
[TBD]


## Quality Attributes
[TBD]


## Constraints
[TBD]


## Principles
[TBD]


## Software Architecture
[TBD]

### Components Diagram
[TBD]

### Container Diagram
[TBD]

## Infrastructure Architecture
[TBD]

### Live Environment
[TBD]


## Deployment
[TBD]

### Software
[TBD]

### Building
[TBD]

### Deploying
[TBD]
### Configuration
[TBD]


## Development
[TBD]

### Environment setup
[TBD]


## Operation & Support
[TBD]

### Starting Services
[TBD]

### Crons
[TBD]

### Monitorization
[TBD]

### Backups
[TBD]